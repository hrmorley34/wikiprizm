---
revisions:
- author: Gbl08ma
  comment: Add OS 02.02
  timestamp: '2016-01-28T00:17:10Z'
title: Versions_and_versioning
---

This page contains information about versions of the Prizm
[OS]({{< ref "OS_Information/" >}}),
[bootloader]({{< ref "CASIOABS.md" >}}), and official add-ins (software
and language), as well as the versioning methods used with them.

[This](http://cdn.knightlab.com/libs/timeline/latest/embed/index.html?source=0AneHm-rN0HqhdDlwTE44b0JZeUJ1eGVUeDdGVS1HQXc&font=Bevan-PotanoSans&maptype=toner&lang=en&height=400)
is a work-in-progress timeline of release and build dates of Prizm
software and add-ins.

## Operating System {#operating_system}

### Versions

#### 01.00

-   Build timestamp: 2010.0922.1713
-   Release date: unknown (same as first fx-CG 10 shipping)
-   Note: this OS version was only available preloaded on fx-CG 10
    models.

#### 01.01 {#section_1}

-   Build timestamp: 2010.1005.1420
-   Release date: unknown
-   Note: this OS version was released at a time when the fx-CG 20 was
    not yet being produced.

#### 01.02 {#section_2}

-   Build timestamp: 2010.1122.2053
-   Release date (preloaded): circa December 2010
-   Release date (online): circa 1st February 2011
-   Note: this OS version was preloaded on the first fx-CG 20 units.
    fx-CG 10 users could update to it as usual.
-   Note: some forum threads point out the existence of a preloaded OS
    that identifies itself as 01.02, but has the Locate bug fixed (see
    [OS 01.03](#01.03) changelog). A dump for this OS never appeared,
    nor any other evidence of its existence, and most likely the bug
    with the Locate command was not correctly reproduced by some users,
    leading to the belief it was corrected.

#### 01.03 {#section_3}

-   Build timestamp: 2011.0608.1049
-   Release date: circa 21st October 2011
-   Changelog:
    -   BASIC command Locate fixed (a problem existed related to the
        color of [multi-byte]({{< ref "Multi-Byte_Strings.md" >}})
        characters).
    -   Memory corruption bug with RclCapt BASIC command fixed.
    -   Exp\>Str() bug fixed.
    -   Freeze or system error with Solve and SolveN commands fixed.
    -   Fixed BASIC "colons within text" bug.
    -   Fixed vertical black line on the right on bitmap screen
        captures.
    -   Possible changes to support big add-in software files, given
        that the Physium add-in (over 1 MB in size) needs this version
        or higher to run.
-   Accompanying releases:
    -   First Physium release for the Prizm.

    :   

#### 01.04 (original release) {#original_release}

-   Build timestamp: 2012.0305.1800
-   Release date: circa 14th March 2012
-   Changelog:
    -   New feature: automatic parentheses insertion.
    -   Improvement of the USB projector connection feature.
    -   Improved system messages (language). Changes are noticeable at
        least in the Portuguese language, with some messages changing
        from their European Portuguese form into Brazilian Portuguese.
-   Accompanying releases:
    -   Version 01.01 of the Geometry add-in.

    :   

#### 01.04 (Macronix flash release) {#macronix_flash_release}

-   Build timestamp: 2012.1009.1425
-   Release date: unknown (first spotted circa 26th October 2013)
-   Changelog:
    -   Minor changes in a reduced number of flash areas, possibly to
        support the Macronix flash chip or the new hardware revision
        ([001V04]({{< ref "Hardware_Revisions.md" >}})) associated with
        it.
-   Note: this release was never available for public download, it only
    came preinstalled. It identifies itself by showing the last digit of
    the version [specializer](#Specializer) as "1" in the end of the
    version code, like "01.04.0001".

#### 02.00 {#section_4}

-   Build timestamp: 2013.1008.1412
-   Release date: circa 4th November 2013
-   Changelog:
    -   Addition of vector support, including vector editor (similar to
        matrix editor) and commands for calculations with vectors.
    -   New RanSamp#, StdDev_Sigma and Variance_Sigma2 commands.
    -   MIXED function for integration areas, on the Graph app.
    -   Solving of underdetermined and overdetermined equation systems.
    -   First OS to officially support language add-ins (although the
        Russian language add-in has been shown to work, with some
        defects, on at least, OS 01.02 on the "fx-CG10/20 Manager"
        emulator).
    -   Built-in support for Spansion and Macronix flash.
    -   Possibly, new USB client drivers (some users have reported that
        Windows informs about new hardware when connecting a OS 02.00
        calculator, that was previously connected with a lower OS
        version).
    -   New system messages for the new features.
    -   Calculators with OS 02.00 are now identified differently on the
        diagnostic menu (a fx-CG 10 displays LY755AZ MAIN instead of
        LY755A MAIN; a fx-CG 20 displays LY755DZ MAIN instead of LY755D
        MAIN). It was previously thought that the "Z" had to do with the
        new hardware revision/flash chip, but this is confirmed not to
        be the case. (Speculating, the "Z" may have to do with new USB
        drivers.)
-   Accompanying releases:
    -   First release (01.00) of the Prob Sim add-in.
    -   Russian language add-in.
    -   Version 01.02 of the Geometry add-in.
    -   Version 01.01 of the Picture Plot add-in.
    -   Version 01.02 of the Physium add-in.

    :   

#### 02.02 {#section_5}

-   Build timestamp: 2015.1127.1657
-   Release date: circa 7th December 2015
-   Changelog:
    -   Built-in app E-Con2 updated to E-Con3
    -   Addition of an
        [examination_mode]({{< ref "examination_mode.md" >}}).
    -   "Improvement of statistical calculations"

    :   

### Versioning

OS versions are usually identified by a major and minor number, two
digits each (including leading zero for values below 10). The OS shows
its version in the form MM.mm.ABCD, where MM is the major, mm is the
minor, and ABCD is a four digits value that varies with the hardware
characteristics, called "specializer" in this Wiki.

Casio appears to respect the meaning of "major" and "minor": between
01.xx releases, the features introduced were not all that important
(independently of fixing serious problems with existing ones), and while
OS 02.00 did not introduce any radical changes, it added some easily
visible and important features.

Each OS build includes a timestamp, presumably of the build time, that
is independent from the OS release date, but usually close to it. The
timestamps are in the form YYYY.MMDD.HHmm, timezone unknown.

#### Specializer

The specializer is a indicator of the characteristics of the OS, without
any notion of precedence from build to build. The same OS code can show
different values for it depending on the hardware, which means that a
different specializer alone isn't enough to identify a different OS
version (it is possible, however, for different OS builds to show the
same major and minor numbers, but different specializers).

On the Prizm at least, the specializer is not stored on the flash: it is
built at run-time for display on the Version screen. For a version
number in the form MM.mm.ABCD, each of the digits A, B, C and D has a
meaning across Casio's line of graphic calculators:

-   First digit (A) - Region
    -   0: No region restrictions
    -   1: Australia
    -   2: France
    -   3: USA and Canada
    -   4: China
    -   5: Singapore

    :   
-   Second digit (B) - Math input/output features
    -   0: n/a
    -   1: Slim (refers to the fx-9860G Slim, which supports "Syntax
        help", but does not have all the math input/output features)
    -   2: All
    -   3: Reduced (fx-9860G, fx-9860GIIs. Some functionality is missing
        like results as multiples of pi)
    -   7: None (e.g., fx-7400GII, fx-9750GII, French Graph 25+ Pro)

    :   
-   Third digit (C) - Build type
    -   0: Standard
    -   1: Development (set in some fx-9860G prototype emulator, also
        set when OS 1.00 is declared final and there are no other
        changes like in the cases of the fx-CG10 and fx-FD10 Pro)

    :   
-   Fourth digit (D) - Build specialty
    -   0: Standard
    -   1: Special (fx-9860GII OSes compiled for the SH-4A hardware,
        Slim OS 1.10, [Macronix release of fx-CG OS
        01.04](#01.04_(Macronix_flash_release)))

    :   

''Thanks to TeamFX for [documenting the specializer
digits](http://www.cemetech.net/forum/viewtopic.php?p=232575#232575).

''

## Bootloader

### Versions {#versions_1}

The only known way to identify a official [bootloader](bootloader) is
using its build timestamp. The code is always the same, and there do not
appear to be separate fx-CG 10 and fx-CG 20 branches. The two
bootloaders built on 10th September 2010 have the same checksum, which
could be the result of careful engineering (for an unknown reason), or
simply mere coincidence.

The bootloaders on the "fx-CG10/20 Manager" emulators have different
timestamps, not listed here, and possibly different code.

#### 2010.0910.1621 {#section_6}

-   Checksum: 0x00 0xC3 0x5C 0x28

#### 2010.0910.1720 {#section_7}

-   Checksum: 0x00 0xC3 0x5C 0x28

#### 2010.0913.0851 {#section_8}

-   Checksum: 0x00 0xC3 0x5C 0x2F

#### 2010.0916.0917 {#section_9}

-   Checksum: 0x00 0xC3 0x5C 0x35

### Versioning {#versioning_1}

The only known way to identify a official [bootloader](bootloader) is
using its build timestamp.

## Official add-in software {#official_add_in_software}

### Versions {#versions_2}

#### Geometry

##### 01.00 {#section_10}

-   Build timestamp: 2010.0907.1250
-   Released: with OS 01.00

##### 01.01 {#section_11}

-   Build timestamp: 2012.0214.0918
-   Released: with OS 01.04

##### 01.02 {#section_12}

-   Build timestamp: 2013.0808.1855
-   Released: with OS 02.00

#### Picture Plot {#picture_plot}

##### 01.00 {#section_13}

-   Build timestamp: 2010.0903.1652
-   Released: with OS 01.00

##### 01.01 {#section_14}

-   Build timestamp: 2013.0905.1347
-   Released: with OS 02.00

#### Conversion

##### 01.00 {#section_15}

-   Build timestamp: 2010.0813.0920
-   Released: with OS 01.00

#### Physium

##### 01.01 {#section_16}

-   Build timestamp: 2011.1017.1523
-   Released: with OS 01.03
-   Note: will not run on OS versions below 01.03

##### 01.02 {#section_17}

-   Build timestamp: 2013.0808.1902
-   Released: with OS 02.00
-   Note: will not run on OS versions below 01.03

#### Prob Sim {#prob_sim}

##### 01.00 {#section_18}

-   Build timestamp: 2013.1016.0812
-   Released: with OS 02.00

### Versioning {#versioning_2}

Version numbers follow a scheme similar to that of the OS. The add-in
file headers have version numbers containing a major, minor and a four
digits value, like OS versions, but these least significant digits are
not shown anywhere on the OS UI. The last four digits are usually, if
not always, set to zero. Version numbers do not always start at 01.00,
as the Physium add-in proves.

## Official add-in languages {#official_add_in_languages}

### Russian

#### Versions {#versions_3}

##### 02.00 {#section_19}

-   Build timestamp: 2013.1015.1651
-   Released: with OS 02.00

Appears to work more or less correctly with OS versions pre-02.00, at
least on the "fx-CG10/20 Manager" emulator with OS 01.02, even though
some strings seem to be mismatched and/or start at wrong places.

### Versioning {#versioning_3}

Version numbers, so far, follow a scheme similar to that of add-ins.
Apparently the first version is not always 01.00 either, because the
first release of the Russian language add-in is version 02.00.
