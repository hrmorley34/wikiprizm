---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= cosh = == Description == This command\nreturns\
    \ the hyperbolic cosine of the value. == Syntax ==\u2019\u2018\u2019cosh\u2019\
    \u2019\u2019\n\u2018\u2019Value\u2019\u2019 == Example == cosh 2\u2019"
  timestamp: '2012-02-29T20:24:16Z'
title: cosh
---

# cosh

## Description

This command returns the hyperbolic cosine of the value.

## Syntax

**cosh** *Value*

## Example

`cosh 2`
