---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-17T00:59:30Z'
title: maxX
---

# maxX

## Description

This is for specifying Maximum x value in x axis at graphscreen.

## Syntax

*Value*→**MaxX**

## Example

`3→MaxX`
