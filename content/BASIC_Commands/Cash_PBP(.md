---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cash_PBP( = == Description == This\ncommand\
    \ returns the payback period. == Syntax\n==\u2019\u2018\u2019Cash_PBP(\u2019\u2019\
    \u2019\u2018\u2019I%\u2019\u2018,\u2019\u2018Cash\u2019\u2019\u2019\u2019\u2018\
    )\u2019\u2019\u2019 == Example ==\nCash_PBP(4.5,3000) \\[\\[Category:BASIC_C\u2026\
    \u2019"
  timestamp: '2012-02-15T23:52:18Z'
title: Cash_PBP(
---

# Cash_PBP(

## Description

This command returns the payback period.

## Syntax

**Cash_PBP(***I%*,*Cash***)**

## Example

`Cash_PBP(4.5,3000)`
