---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Return = == Description == This command\nis\
    \ used to return to main program from the subprogram. == Syntax\n==\u2019\u2018\
    \u2019Return\u2019\u2019\u2019 == Example == Return\n\\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-22T03:18:15Z'
title: Return
---

# Return

## Description

This command is used to return to main program from the subprogram.

## Syntax

**Return**

## Example

`Return`
