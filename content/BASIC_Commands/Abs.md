---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Abs = == Description == This command\nreturns\
    \ the absolute value of value. == Syntax ==\u2019\u2018\u2019Abs\u2019\u2019\u2019\
    \ \u2018\u2019Value\u2019\u2019\n== Example == Abs -3 \\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-15T03:41:01Z'
title: Abs
---

# Abs

## Description

This command returns the absolute value of value.

## Syntax

**Abs** *Value*

## Example

`Abs -3`
