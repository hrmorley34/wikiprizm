---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= AxesOff = == Description == This\ncommand sets\
    \ graph screen to hide axes. == Syntax ==\u2019\u2018\u2019AxesOff\u2019\u2019\
    \u2019 ==\nExample == AxesOff \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T12:10:43Z'
title: AxesOff
---

# AxesOff

## Description

This command sets graph screen to hide axes.

## Syntax

**AxesOff**

## Example

`AxesOff`
