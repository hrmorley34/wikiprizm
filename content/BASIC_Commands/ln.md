---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Ln = == Description == This command\nreturns\
    \ the natural logarithm of a value. == Syntax ==\u2019\u2018\u2019ln\u2019\u2019\
    \u2019\n\u2018\u2019Value\u2019\u2019 == Example == ln 3 \\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-22T03:19:18Z'
title: ln
---

# Ln

## Description

This command returns the natural logarithm of a value.

## Syntax

**ln** *Value*

## Example

`ln 3`
