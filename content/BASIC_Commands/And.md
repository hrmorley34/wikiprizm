---
revisions:
- author: KermMartian
  timestamp: '2011-12-04T15:47:07Z'
title: And
---

## Description

This command is placed in between two conditionals, generally when one
needs to test if boundaries are reached with more than one variable.

## Syntax

(condition 1) **And** (condition 2)

## Example

    If K=31 And A>0
    While J>20 And J<50
    A-5((K=27 And B=1)-(K=28 And B=2->A
