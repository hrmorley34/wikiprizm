---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= r(rad) = == Description == This command\nspecifies\
    \ the evaluation in radian mode. == Syntax ==\u2019\u2018Expression\u2019\u2019\
    \n\u2019\u2018\u2019r\u2019\u2019\u2019 == Example == sin 30r \\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-24T20:35:09Z'
title: R(rad)
---

# r(rad)

## Description

This command specifies the evaluation in radian mode.

## Syntax

*Expression* **r**

## Example

`sin 30r`
