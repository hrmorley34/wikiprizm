---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= d/c = == Description == This command\nsets the\
    \ calculator to display the answer as d/c format if possible.\n== Syntax ==\u2019\
    \u2018\u2019d/c\u2019\u2019\u2019 == Example == d/c\n\\[\\[Category:BASIC_Commands\u2026\
    \u2019"
  timestamp: '2012-02-15T20:18:26Z'
title: "D\u2215c"
---

# d/c

## Description

This command sets the calculator to display the answer as d/c format if
possible.

## Syntax

**d/c**

## Example

`d/c`
