---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cash_NPV( = == Description == This\ncommand\
    \ returns the net present value. == Syntax\n==\u2019\u2018\u2019Cash_NPV(\u2019\
    \u2019\u2019\u2018\u2019I%\u2019\u2018,\u2019\u2018Cash\u2019\u2019\u2019\u2019\
    \u2018)\u2019\u2019\u2019 == Example ==\nCash_NPV(4.5,3000) \\[\\[Category:BASI\u2026\
    \u2019"
  timestamp: '2012-02-15T23:51:11Z'
title: Cash_NPV(
---

# Cash_NPV(

## Description

This command returns the net present value.

## Syntax

**Cash_NPV(***I%*,*Cash***)**

## Example

`Cash_NPV(4.5,3000)`
