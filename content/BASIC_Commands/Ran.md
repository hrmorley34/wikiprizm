---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Ran# = == Description == This command\nreturns\
    \ the random non-integer number between 0 and 1. == Syntax\n==\u2019\u2018\u2019\
    Ran#\u2019\u2019\u2019 == Example == Ran# \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-21T22:50:07Z'
title: Ran
---

# Ran#

## Description

This command returns the random non-integer number between 0 and 1.

## Syntax

**Ran#**

## Example

`Ran#`
