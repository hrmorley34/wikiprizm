---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= \u222B( = == Description == This command\ncalculates\
    \ the integral of speficied function respect to speficied\nvariable, within the\
    \ range of a and b. == Syntax\n==\u2019\u2019\u2018\u222B(\u2019\u2019\u2019\u2018\
    \u2019Functio\u2026\u2019"
  timestamp: '2012-02-15T20:28:07Z'
title: "\u222B("
---

# ∫(

## Description

This command calculates the integral of speficied function respect to
speficied variable, within the range of a and b.

## Syntax

**∫(***Function*,*variable*,*low_range*,*hi_range***)**

## Example

`∫(3x^2+5,x,1,3`
