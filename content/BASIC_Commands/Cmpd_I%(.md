---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cmpd_I%( = == Description == This\ncommand returns\
    \ annual interest. == Syntax\n==\u2019\u2018\u2019Cmpd_I%(\u2019\u2019\u2019\u2018\
    \u2019n\u2019\u2018,\u2019\u2018PV\u2019\u2018,\u2019\u2018PMT\u2019\u2018,\u2019\
    \u2018FV\u2019\u2018,\u2019\u2018P/Y\u2019\u2018,\u2019\u2018C/Y\u2019\u2019\u2019\
    \u2019\u2018)\u2019\u2019\u2019\n== Example == Cmpd_I%(10,1000\u2026\u2019"
  timestamp: '2012-02-16T00:10:10Z'
title: Cmpd_I%(
---

# Cmpd_I%(

## Description

This command returns annual interest.

## Syntax

**Cmpd_I%(***n*,*PV*,*PMT*,*FV*,*P/Y*,*C/Y***)**

## Example

`Cmpd_I%(10,1000,10,0,12,12)`
