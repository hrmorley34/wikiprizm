---
revisions:
- author: JosJuice
  comment: 'Writing the text in a different location makes more sense in

    an example program.'
  timestamp: '2012-01-23T19:57:20Z'
title: Locate
---

## Description

This command is used to display text on the 'homescreen'; a color
command can be used to denote the color of drawn text.

## Syntax

```
Locate [column #1-21],[row #1-7],"TEXT
```

or

```
<color> Locate [column #1-21],[row #1-7],"TEXT
```

## Example

     Locate 1,1,"HELLO WORLD!
     Red Locate 1,2,"HELLO WORLD!
