---
revisions:
- author: Turiqwalrus
  timestamp: '2012-02-29T20:17:59Z'
title: PxlOff
---

## PxlOff

## Description

This command turns a pixel on the graph screen off.

## Syntax

`PxlOff <X-coordinate>,<Y-Coordinate>`

## Example

`PxlOff 5,5`
