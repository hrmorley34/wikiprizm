---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= InvGeoCD( = == Description == This\ncommand\
    \ returns the x value from probability p and success rate P.\n== Syntax ==\u2019\
    \u2018\u2019InvGeoCD(\u2019\u2019\u2019\u2018\u2019p\u2019\u2018,\u2019\u2018\
    P\u2019\u2019\u2019\u2019\u2018)\u2019\u2019\u2019 == Example ==\nInvGeoCD(.01\u2026\
    \u2019"
  timestamp: '2012-02-16T00:27:55Z'
title: InvGeoCD(
---

# InvGeoCD(

## Description

This command returns the x value from probability p and success rate P.

## Syntax

**InvGeoCD(***p*,*P***)**

## Example

InvGeoCD(.017,.6)
