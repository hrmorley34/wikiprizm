---
revisions:
- author: Purobaz
  comment: "Created page with \u2018= Magenta = == Description == This command\nsets\
    \ the subsequent Locate or other text command to render in\nMagenta. This command\
    \ can be found at \\[Shift\\] \\[5\\] \\[1\\] \\[4\\].\n== Syntax == \u2026\u2019"
  timestamp: '2012-02-22T22:27:11Z'
title: Magenta
---

# Magenta

## Description

This command sets the subsequent Locate or other text command to render
in Magenta. This command can be found at \[Shift\] \[5\] \[1\] \[4\].

## Syntax

**Magenta \<text command>**

## Example

`Magenta Locate 1, 1, "Asdf`
