---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cmpd_PMT( = == Description == This\ncommand\
    \ returns the equal input/output values for a fixed period. ==\nSyntax\n==\u2019\
    \u2018\u2019Cmpd_PMT(\u2019\u2019\u2019\u2018\u2019n\u2019\u2018,\u2019\u2018\
    I%\u2019\u2018,\u2019\u2018PV\u2019\u2018,\u2019\u2018FV\u2019\u2018,\u2019\u2018\
    P/Y\u2019\u2018,\u2019\u2018C/Y\u2019\u2019\u2019\u2019\u2018)\u2019\u2026\u2019"
  timestamp: '2012-02-16T00:12:04Z'
title: Cmpd_PMT(
---

# Cmpd_PMT(

## Description

This command returns the equal input/output values for a fixed period.

## Syntax

**Cmpd_PMT(***n*,*I%*,*PV*,*FV*,*P/Y*,*C/Y***)**

## Example

`Cmpd_PMT(10,5.5,1000,0,12,12)`
