---
revisions:
- author: Purobaz
  timestamp: '2012-02-22T22:36:58Z'
title: BG-Pict
---

# BG-Pict {#bg_pict}

## Description

This command sets a picture in background. But it doesn't draw the
screen.

## Syntax

**BG-Pict**

## Example

    BG-Pict 1
    Cls
