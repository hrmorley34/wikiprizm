---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= ^10 = == Description == This command\nreturns\
    \ the value^10. == Syntax ==\u2019\u2018Value\u2019\u2019\u2019\u2019\u2018^10\u2019\
    \u2019\u2019 == Example ==\n2^10 \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-24T20:16:09Z'
title: ^10
---

# ^10

## Description

This command returns the value^10.

## Syntax

*Value***^10**

## Example

`2^10`
