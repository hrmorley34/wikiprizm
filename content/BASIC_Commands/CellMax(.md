---
revisions:
- author: YeongJIN COOL
  comment: /\* Syntax \*/
  timestamp: '2012-02-16T00:03:11Z'
title: CellMax(
---

# CellMax(

## Description

This command returns the maximum value in a specified cell range.

## Syntax

**CellMax(***start_cell*:*end_cell***)**

## Example

`CellMax(A3:C5)`
