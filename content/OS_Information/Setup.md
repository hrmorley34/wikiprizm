---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-04T20:02:32Z'
title: Setup
aliases:
  - /Setup/
---

The [OS]({{< ref "_index.md" >}}) uses a table of settings, usually
called *setup*, that is accessible with the syscalls
[GetSetupSetting]({{< ref "Syscalls/GetSetupSetting.md" >}}) and
[SetSetupSetting]({{< ref "Syscalls/SetSetupSetting.md" >}}). Most OS
settings are stored in this table. The setup appears to the user as if
it was stored in the [MCS]({{< ref "File_System.md" >}}) item **SETUP**,
but it is not actually stored in the MCS - it is stored in RS memory,
address ranges 0xFD80xxxx (on the "fx-CG10/20 Manager" emulator, it
starts at 0xFD801060).

The values are OS-wide, which means that changing a setting in one
application changes it for all applications. The exception is eActivity
files and strips: files and strips inside those files each have a
separate setup.

On the Prizm, the setup table has space for 200 entries.

A limited setup editor can be accessed from most built-in OS
applications by pressing SET UP (Shift+Menu). The options are limited to
what makes sense in the currently running app - for example, the "Stat
Wind" option is available on the Statistics app, but not on Run-Matrix.
From the [TestMode]({{< ref "Syscalls/TestMode.md" >}}), one can access
a setup editor that lists almost all options. There are some options,
such as the keyboard input mode (Shift, Alpha, Clip, etc.) which are
present in the setup but don't show in any of the editors, not even the
TestMode one.

## Settings list {#settings_list}

| Index | Meaning and values                                                                                                                                                                           |
|-------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0     | Unknown                                                                                                                                                                                      |
| 1     | Unknown                                                                                                                                                                                      |
| 2     | Unknown                                                                                                                                                                                      |
| 3     | Unknown                                                                                                                                                                                      |
| 4     | Unknown                                                                                                                                                                                      |
| 5     | Unknown                                                                                                                                                                                      |
| 6     | Unknown                                                                                                                                                                                      |
| 7     | Unknown                                                                                                                                                                                      |
| 8     | Unknown                                                                                                                                                                                      |
| 9     | Unknown                                                                                                                                                                                      |
| 10    | Unknown                                                                                                                                                                                      |
| 11    | Unknown                                                                                                                                                                                      |
| 12    | Unknown                                                                                                                                                                                      |
| 13    | Unknown                                                                                                                                                                                      |
| 14    | Unknown                                                                                                                                                                                      |
| 15    | Unknown                                                                                                                                                                                      |
| 16    | Unknown                                                                                                                                                                                      |
| 17    | Unknown                                                                                                                                                                                      |
| 18    | Unknown                                                                                                                                                                                      |
| 19    | Angle unit. 0 for Degrees, 1 for Radians, 2 for Gradians.                                                                                                                                    |
| 20    | Keyboard input mode. 0 for standard, 0x01 for Shift, 0x02 for Clip, 0x04 for Alpha, 0x08 for lower-case <br/><br/>Alpha, <br/><br/><br/>0x84 for Alpha-lock, 0x88 for lower-case Alpha-Lock. |
| 21    | Keyboard insert/overwrite mode. 1 for overwrite, 2 for insert.                                                                                                                               |
| 22    | Unknown                                                                                                                                                                                      |
| 23    | Mode. 0 for Comp, 1 for Bin, 7 for Oct, 9 for Dec, 15 for Hex.                                                                                                                               |
| 24    | Function type. 0 for Y=, 1 for r=, 2 for Param, 3 for X=, 4 for Y\>, 5 for Y\<, 6 for Y\>=, 7 for Y\<=, 8 for X\>, 9 for X\<, 10 for X\>=, 11 for X\<=.                                      |
| 25    | Draw type. 0 for Connect, 1 for Plot.                                                                                                                                                        |
| 26    | Derivative. 0 for On, 1 for Off.                                                                                                                                                             |
| 27    | Coordinates. 0 for On, 1 for Off.                                                                                                                                                            |
| 28    | Grid. 0 for On, 1 for Off, 3 for Line.                                                                                                                                                       |
| 29    | Axes. 0 for On, 1 for Off, 3 for Scale.                                                                                                                                                      |
| 30    | Label. 0 for On, 1 for Off.                                                                                                                                                                  |
| 31    | Unknown.                                                                                                                                                                                     |
| 32    | Statistics Window. 0 for Auto, 1 for Manual.                                                                                                                                                 |
| 33    | Graph Function. 0 for On, 1 for Off.                                                                                                                                                         |
| 34    | Dual Screen. 0 for Graph+Graph, 1 for Graph to Table, 3 for Off.                                                                                                                             |
| 35    | Simultaneous Graphs. 0 for On, 1 for Off.                                                                                                                                                    |
| 36    | Dynamic Type. 0 for Cont, 1 for Stop.                                                                                                                                                        |
| 37    | Σ Display. 0 for On, 1 for Off.                                                                                                                                                              |
| 38    | Slope. 0 for On, 1 for Off.                                                                                                                                                                  |
| 39    | Payment. 0 for Begin, 1 for End.                                                                                                                                                             |
| 40    | Date Mode. 0 for 365, 1 for 360.                                                                                                                                                             |
| 41    | Answer Type. 0 for Real, 1 for Complex.                                                                                                                                                      |
| 42    | Complex Mode. 0 for Real, 1 for a+bi, 2 for r\<T.                                                                                                                                            |
| 43    | Display. 0 for Norm1, 16 for Norm2, 32 to 39 for Fix 0 to 9, 48 to 57 for Sci 0 to 9. Add 128 to the values to enable Eng mode.                                                              |
| 44    | Background. 0 for None, 1 to 20 for Pict1...20, 21 for user-selected g3p file.                                                                                                               |
| 45    | Resid List. 0 for None, 1 to 26 for List1...26.                                                                                                                                              |
| 46    | List File. 1 to 6 for File1...6.                                                                                                                                                             |
| 47    | Variable. 0 for Range, 1 to 26 for List1...26.                                                                                                                                               |
| 48    | Recursion Type. 0 for a<sub>n</sub>, 1 for a<sub>n+1</sub>, 3 for a<sub>n+2</sub>.                                                                                                           |
| 49    | Dual Screen (for recursion). 0 for Table+Graph, 1 for Off.                                                                                                                                   |
| 50    | Unknown                                                                                                                                                                                      |
| 51    | Unknown                                                                                                                                                                                      |
| 52    | Unknown                                                                                                                                                                                      |
| 53    | Unknown                                                                                                                                                                                      |
| 54    | Unknown                                                                                                                                                                                      |
| 55    | Unknown                                                                                                                                                                                      |
| 56    | Unknown                                                                                                                                                                                      |
| 57    | Unknown                                                                                                                                                                                      |
| 58    | Unknown                                                                                                                                                                                      |
| 59    | Unknown                                                                                                                                                                                      |
| 60    | Unknown                                                                                                                                                                                      |
| 61    | Unknown                                                                                                                                                                                      |
| 62    | Unknown                                                                                                                                                                                      |
| 63    | Unknown                                                                                                                                                                                      |
| 64    | Unknown                                                                                                                                                                                      |
| 65    | Unknown                                                                                                                                                                                      |
| 66    | Unknown                                                                                                                                                                                      |
| 67    | Unknown                                                                                                                                                                                      |
| 68    | Unknown                                                                                                                                                                                      |
| 69    | Unknown                                                                                                                                                                                      |
| 70    | Unknown                                                                                                                                                                                      |
| 71    | Unknown                                                                                                                                                                                      |
| 72    | Unknown                                                                                                                                                                                      |
| 73    | Unknown                                                                                                                                                                                      |
| 74    | Unknown                                                                                                                                                                                      |
| 75    | Unknown                                                                                                                                                                                      |
| 76    | Unknown                                                                                                                                                                                      |
| 77    | Unknown                                                                                                                                                                                      |
| 78    | Auto Calc. 0 for On, 1 for Off.                                                                                                                                                              |
| 79    | Show Cell. 0 for Formula, 1 for Value.                                                                                                                                                       |
| 80    | Move. 0 for Lower, 1 for Right.                                                                                                                                                              |
| 81    | Sub Name. 0 for On, 1 for Off.                                                                                                                                                               |
| 82    | Unknown                                                                                                                                                                                      |
| 83    | Input Mode. 0 for Math, 1 for Linear.                                                                                                                                                        |
| 84    | Locus. 0 for On, 1 for Off.                                                                                                                                                                  |
| 85    | Y=DrawSpeed. 0 for Norm, 1 for High.                                                                                                                                                         |
| 86    | Sketch Line. 0 for Norm, 1 for Thick, 2 for Broken, 3 for Dot, 4 for Thin.                                                                                                                   |
| 87    | Fraction Result. 0 for d/c, 1 for ab/c.                                                                                                                                                      |
| 88    | Unknown                                                                                                                                                                                      |
| 89    | Unknown                                                                                                                                                                                      |
| 90    | Unknown                                                                                                                                                                                      |
| 91    | Unknown                                                                                                                                                                                      |
| 92    | Q1Q3 Type. 0 for Std, 1 for OnData.                                                                                                                                                          |
| 93    | Ineq Type. 0 for Intersect, 1 for Union.                                                                                                                                                     |
| 94    | Periods/Yr. 0 for Annual, 1 for Semi.                                                                                                                                                        |
| 95    | Simplify. 0 for Auto, 1 for Manual.                                                                                                                                                          |
|       |                                                                                                                                                                                              |
