---
revisions:
- author: Krazylegodrummer56
  timestamp: '2013-02-05T01:34:03Z'
title: FKey Bitmaps
aliases:
  - /FKey_Bitmaps/
---

FKey codes used in
[FKey_Display]({{< ref "Syscalls/UI_elements/FKey_Display.md" >}}) and
[GetFKeyPtr]({{< ref "Syscalls/Locale/GetFKeyPtr.md" >}})

| Pre Made F-Keys         |
|-------------------------|
| Blank cells             |
| Norm                    |
| Graph                   |
| Calc                    |
| Test                    |
| Intr                    |
| Dist                    |
| Unfilled play button    |
| SortASC                 |
| SortDES                 |
| Delete                  |
| Yes                     |
| Insert                  |
| Graph1                  |
| Graph2                  |
| Graph3                  |
| Select                  |
| Set                     |
| On                      |
| Off                     |
| Draw                    |
| 1-Var                   |
| 2-Var                   |
| Reg                     |
| Scatter                 |
| xyLine                  |
| NPPlot                  |
| Hist                    |
| MedBox                  |
| Box(with line above it) |
|                         |
