---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Bdisp_WriteGraphVRAM\n\\\
    | index = 0x0290 \\| signature = void Bdisp_WriteGraphVRAM(struct\ndisplay_graph\\\
    * gd) \\| header = fxcg/display.h \\| parameters = \\*\n\u2019\u2019struct\u2026\
    \u201D"
  timestamp: '2014-07-29T21:06:10Z'
title: Bdisp_WriteGraphVRAM
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0290\
**Function signature:** void Bdisp_WriteGraphVRAM(struct display_graph\*
gd)

Draws a bitmap to VRAM, supporting multiple color modes, mask colors,
skipping offsets inside the bitmap, transparency and more.

## Parameters

-   *struct display_graph\** **gd** - pointer to struct containing
    information on the bitmap to draw

    ```c
    struct display_graph {
        int x;
        int y;
        int xofs;
        int yofs;
        int width;
        int height;
        char colormode;
        char zero4;
        char P20_1;
        char P20_2;
        int bitmap;
        char color_idx1;
        char color_idx2;
        char color_idx3;
        char P20_3;
        char writemodify;
        char writekind;
        char zero6;
        char one1;
        int transparency;
    }
    ```

-   **x** and **y** - position in pixels where the bitmap should be
    drawn (coordinates of the top-left corner).
-   **xofs** and **yofs** - offset inside the bitmap, where its drawing
    should start.
-   **width** and **height** - dimension, in pixels, of the bitmap to
    draw.
-   **colormode** - 1 is 4 bit-per-pixel, 16 color mode; 2 is 16
    bit-per-pixel, full color (RGB565); 3 is 1 bit-per-pixel,
    monochrome, 16 color mode.
-   **bitmap** - pointer to the bitmap to draw.
-   **color_idx1** - indexed color (see
    [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}) colors), which is used
    to draw a set bit when **colormode** is 3.
-   **color_idx2** - indexed color, which is used to draw a cleared bit
    when **colormode** is 3.
-   **color_idx3** - indexed color, which is transparent when
    **colormode** is 3.
-   **writemodify** - 1 is normal, 2 is reverse, 3 is halftone
-   **writemode** - 1 overwrites, 2 OR-writes, 3 AND-writes and 4
    XOR-writes.
-   **transparency** - value from 0 to 100 specifying the transparency.
-   **zero4**, **zero6** and **one1** should be set to 0 (zero).
-   **P20_1** and **P20_2** should be set to 0x20.
-   **P20_3** should be set to 1.

## Comments

Unscientific tests showed that using Bdisp_WriteGraphVRAM to write 16
bit-per-pixel color bitmaps (to achieve the same purpose as the custom
function [CopySprite]({{< ref "Useful_Routines/CopySprite.md" >}})) is
as fast, or even faster, than using CopySprite, with GCC 4.8 optimizing
for size. This syscall (or a variant of it) probably also supports mask
colors on 16 bpp bitmaps, because file type icons, in native file
browsers, must be drawn in some way. If such functionality is found,
then this syscall will be a valid replacement for the custom functions
usually known as CopySprite (already is), XORSprite (already is),
CopySpriteMasked, AlphaSprite, CopySpriteMaskedAlpha, CopySprite1bit,
CopySprite3bit and possibly more.
