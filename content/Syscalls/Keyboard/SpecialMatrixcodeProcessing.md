---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:20:13Z'
title: SpecialMatrixcodeProcessing
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1E60\
**Function signature:** void SpecialMatrixcodeProcessing(int\* col,
int\* row)

Processes a special matrix code as if the respective key, such as the OS
update contact, had been pressed.

## Parameters

-   **col** - pointer to integer containing the column number of the key
    matrix code to process;
-   **row** - pointer to integer containing the row number of the key
    matrix code to process.

## Example

Opening the diagnostic menu accessed with OPTN+EXP+AC/ON:

       int row = 0xC, col = 4;  // DIAG - contact
       SetSetupSetting( 0x14, 0 ); // turn Shift/Alpha mode off
       SpecialMatrixcodeProcessing( &col, &row );

Opening the OS update dialog:

       int row = 0xC, col = 2;  // OSUP - contact
       SetSetupSetting( 0x14, 0 ); // turn Shift/Alpha mode off
       SpecialMatrixcodeProcessing( &col, &row );

Switching the calculator off (for demonstration only, using
[PowerOff]({{< ref "Syscalls/PowerOff.md" >}}) is certainly easier):

       int row = 1, col = 1;  // ACon - key
       SetSetupSetting( 0x14, 1 ); // turn Shift mode on
       SpecialMatrixcodeProcessing( &col, &row );
