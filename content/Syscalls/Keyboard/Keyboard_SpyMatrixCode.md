---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:19:05Z'
title: Keyboard_SpyMatrixCode
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x12C9\
**Function signature:** int Keyboard_SpyMatrixCode(char\* column, char\*
row)

Gets the next matrix code available in the key buffer, without removing
it from the buffer.

## Parameters

-   **column** - pointer to `char` that will receive the column number
    of the matrix code;
-   **row** - pointer to `char` that will receive the row number of the
    matrix code.

## Returns

The current number of key codes in the key buffer.

## Comments

As noted above, this syscall doesn't remove the key from the key buffer,
hence the "Spy" in the name.
