---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:13:18Z'
title: APP_SYSTEM_VERSION
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1E34\
**Function signature:** void APP_SYSTEM_VERSION(void);

Opens the "Version" screen that shows in the System menu (when F4 is
pressed).

## Comments

The syscall returns when the user presses EXIT.
