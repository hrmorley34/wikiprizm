---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-15T22:53:40Z'
title: GetFKeyPtr
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x12F3\
**Function signature:** *void* **GetFKeyPtr**(*int* **id**, *void\**
**result**)

This function tells you the position of a requested [FKey
icon](FKey_bitmap) in the currently selected [language for function key
labels]({{< ref "/OS_Information/Locale.md" >}}).

## Parameters

-   *int* **id** - The ID (between 0 and 0x04E0) of the FKey bitmap
    stored in the memory. Use Insight's *FKey icons* option, or the
    [TestMode]({{< ref "Syscalls/TestMode.md" >}}), to find the ID for
    an Fkey icon.
-   *void\** **result** - Pointer to an integer variable which can be
    used with
    [FKey_Display]({{< ref "Syscalls/UI_elements/FKey_Display.md" >}}).

## Returns

The function sets the given variable as a pointer to the start of the
requested bitmap.

## Example

This code displays "TEST" above F1 button:

    int iresult;
    GetFKeyPtr(0x0003, &iresult);
    FKey_Display(0, iresult);
