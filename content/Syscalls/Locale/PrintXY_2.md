---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-15T22:31:32Z'
title: PrintXY_2
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x18EC\
**Function signature:** void PrintXY_2(int mode, int x, int y, int
msgno, int color)

Displays default messages in the language that the calculator owner has
selected in the Language screen available from the System menu.

## Parameters

-   **mode** - see [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}) for a
    list of valid modes.
-   **x**, **y** - where to draw the message, in text character
    coordinates.
-   **msgno** - number of the message to display. To see all the
    messages and their numbers, use the
    [TestMode]({{< ref "Syscalls/TestMode.md" >}}) or an add-in like
    [this
    one](http://www.cemetech.net/scripts/countdown.php?/prizm/tools/PrintXY_2_Ref.zip&path=archives).
    See below for more information.
-   **color** - text color to draw the message in, see
    [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}) for a list of valid
    colors.

Here's a list with some of the messages, which is not in any way
representative of the full list with thousands of messages:

| msgno | output                          |
|-------|---------------------------------|
| 1     | Displays the selected language. |
| 2     | Press:\[EXIT\]                  |
| 3     | Yes:\[F1\]                      |
| 4     | No:\[F6\]                       |
| 5     | Store In                        |
| 6     | Recall From                     |
| 7     | Select                          |
| 8     | Delete Formula?                 |
| 10    | Press \[MENU\] KEY              |
| 11    | Draw: \[EXE\]                   |
| 12    | Execute                         |
| 13    | V-Win Memory                    |
| 15    | Graph Memory                    |
| 17    | Dynamic Memory                  |
| 19    | Picture Memory                  |
| 21    | Function Memory                 |
| 23    | List Memory                     |
| 33    | RESET (CLEAR)                   |
| 34    | MAIN MEMORY?                    |
| 36    | MAIN MEMORY                     |
| 37    | CLEARED!                        |
| 40    | RESET (CLEAR)                   |
| 41    | STORAGE MEMORY?                 |
| 44    | INITIALIZE?                     |
| 47    | Low                             |
| 48    | Batteries!                      |
| 50    | Please Replace                  |
| 52    | Low                             |
| 53    | Backup Battery!                 |
| 55    | Please Replace                  |
| 57    | Break                           |
| 59    | Terminate                       |
| 60    | Application?                    |
| 68    | Condition ERROR                 |
| 71    | Time Out                        |
| 74    | System ERROR                    |
| 77    | Memory ERROR                    |
| 80    | Syntax ERROR                    |
| 83    | Ma ERROR                        |

THERE ARE *MANY* MORE. THIS IS NOT A COMPLETE LIST.
