---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-15T22:33:12Z'
title: Bdisp_WriteSystemMessage
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1906\
**Function signature:** void Bdisp_WriteSystemMessage(int x, int y, int
msgno, int mode, char color3)

Draws a message to VRAM from the built-in messages list, in the language
selected on the Language screen of the System menu. Similar to
[PrintXY_2]({{< ref "Syscalls/Locale/PrintXY_2.md" >}}).

## Parameters

x and y are probably pixel coordinates rather than "homescreen"
coordinates; for information on the other parameters see
[PrintXY_2]({{< ref "Syscalls/Locale/PrintXY_2.md" >}}).
