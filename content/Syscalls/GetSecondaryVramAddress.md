---
revisions:
- author: Dr-carlos
  comment: "Remove GetSecondaryVramAddress \u2018not yet in libfxcg\u2019"
  timestamp: '2022-03-17T09:21:37Z'
title: GetSecondaryVramAddress
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1E50\
**Function signature:** void \*getSecondaryVramAddress(void);

## Returns

The address used by the
[SaveVRAM_1]({{< ref "Syscalls/SaveVRAM_1.md" >}}) and
[LoadVRAM_1]({{< ref "Syscalls/LoadVRAM_1.md" >}}) functions.

## Comments

The syscall 0x1B0B contains identical code to this syscall. 0x1E50 is
used more often in internal OS code.
