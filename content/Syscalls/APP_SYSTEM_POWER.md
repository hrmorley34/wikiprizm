---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:10:28Z'
title: APP_SYSTEM_POWER
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1E05\
**Function signature:** void APP_SYSTEM_POWER(int opt);

Opens the "Power Properties" screen that shows in the start-up wizard
and in the System menu (when F2 is pressed).

## Parameters

-   **opt** - Controls whether the screen should behave like in the
    wizard (1), or like in the System menu (0).

## Comments

The settings for the power timeouts actually change when an option is
selected, even on the "fx-CG10/20 Manager" emulator.

If the value of the parameter is zero, the syscall returns when the user
presses EXIT. If the value of the parameter is 1, it returns when the
user presses F6.
