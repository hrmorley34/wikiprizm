---
revisions:
- author: Ahelper
  comment: libfxcg API
  timestamp: '2014-11-23T00:28:20Z'
title: PrintLine
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1883\
**Function signature:** void PrintLine(const char\*msg, int imax)

Prints a message on the screen at the cursor position set by
[locate_OS]({{< ref "Syscalls/locate_OS.md" >}}), cutting it at a
certain length.

## Parameters

-   **msg** - pointer to a buffer (possibly `const` and/or `static`)
    containing the message to display.
-   **imax** - maximum length, zero-based, of the message to display.
    Characters past this length will not be displayed.

## Comments

This syscall calls [PrintLine2]({{< ref "Syscalls/PrintLine2.md" >}})
like this: `PrintLine2(0, 1, msg, 0, -1, imax, 0, 7)`.

## Example

The following code:


    locate_OS(2,3);
    PrintLine((unsigned char*)"ThisIsAtest", 5);

...will display "This" starting at the text column 2, row 3.
