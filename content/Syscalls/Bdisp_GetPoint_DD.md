---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T11:27:46Z'
title: Bdisp_GetPoint_DD
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x026F\
**Function signature:** unsigned short Bdisp_GetPoint_DD(int x, int y);

Get the value of a pixel on screen, bypassing VRAM.

## Parameters

-   **x**: pixel column, 0 to 383.
-   **y**: pixel row, 0 to 215.

## Returns

-   The value of the pixel at (x,y) on the screen, which is a [5-6-5 RGB
    triple]({{< ref "Technical_Documentation/display.md" >}}).

## Comments

The color returned by this syscall is the one currently displayed on the
screen, independently of the VRAM contents.
