---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T12:19:57Z'
title: WriteBackground
---

## Synopsis

**Header:** fxcd/display.h\
**Syscall index:** 0x1EF7\
**Function signature:** void WriteBackground(void\* target, int width,
int height, void\* source, int, int, int )

Sets a background image from a given bitmap, unlike
[SetBackGround]({{< ref "Syscalls/UI_elements/SetBackGround.md" >}})
which allows you to set a predefined background from the default
included in the OS.

## Parameters

-   *void\** **target** - pointer to the destination of the bitmap
-   *int* **width** - should be set to 384
-   *int* **height** - should be set to 192
-   *void\** **source** - pointer to a TBGRbitmap struct.

The last three parameters should be set to zero.

    typedef struct {
      unsigned short width;    // set to 384
      unsigned short height;   // set to 192
      unsigned short unknown1; // set to 0x10
      unsigned short unknown2; // set to 0x00
      unsigned short unknown3; // set to 0x01
      unsigned short unknown4; // set to 0xF800
      unsigned short* bitmap;  // pointer to the bitmap to set as background
    } TBGRbitmap
