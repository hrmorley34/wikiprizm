---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-10T23:00:41Z'
title: RTC_SetDateTime
---

## Synopsis

**Header:** fxcg/rtc.h\
**Syscall index:** 0x11DE\
**Function signature:** void RTC_SetDateTime(unsigned char\* timestr)

Sets the Real-Time Clock.

## Parameters

-   **timestr** - Pointer to 7 bytes `unsigned char` array. The meaning
    of each array field is as follows:
    -   **timestr**\[0\] and **timestr**\[1\]: year in BCD format (for
        example, 0x20 0x08);
    -   **timestr**\[2\]: month in BCD format (for example, 0x03);
    -   **timestr**\[3\]: day in BCD format (for example, 0x02);
    -   **timestr**\[4\]: hour in BCD format (for example, 0x19);
    -   **timestr**\[5\]: minute in BCD format (for example, 0x47);
    -   **timestr**\[6\]: second in BCD format (for example, 0x03).
