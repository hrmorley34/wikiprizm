---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:14:56Z'
title: ResetAllDialog
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1E23\
**Function signature:** void ResetAllDialog(void);

Opens a message box asking "Reset OK?" on the first line and saying
"Initialize All" on the second, same as pressing F6 then F2 on the
"RESET" screen available from the System menu.

## Comments

If the user answers with F6 or EXIT, the syscall will return. If F1 is
pressed and add-ins are installed, a new question will appear, "Delete
Add-ins?","(Add-ins are","after SYSTEM","on MAIN MENU.)". If EXIT or F6
is pressed on this second screen, the syscall returns and no reset
operation is performed. If F1 is pressed (or if there are no add-ins
installed), the system will actually empty all memories (be careful) and
display a "Reset!","Initialize All" message. Once EXIT is pressed the
calculator will reboot, however, one can still get to the Main Menu
while this message is shown.
