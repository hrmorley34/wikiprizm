---
revisions:
- author: Gbl08ma
  timestamp: '2014-12-05T18:56:35Z'
title: EnableStatusArea
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x02B7\
**Function signature:** void EnableStatusArea(int opt)

Enables or disables the status area.

## Parameters

-   **opt** - If 0 or 2, enables the status area. If 1, no operation is
    performed. If 3, disables the status area.

## Comments

Influences the int system variable at 0x8804F52C (fx-CG20 OS
01.02.0200). On OS 02.00 it is now 0x8804F5EC. You should not attempt to
use these addresses directly to enable or disable the status area,
because as one can see, the value changes its location in memory between
versions of the OS.
