---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Keyboard_CursorFlash\n\\\
    | index = 0x08CA \\| signature = void Keyboard_CursorFlash(void) \\|\nheader =\
    \ fxcg/display.h \\| synopsis = The function of this syscall\nis n\u2026\u201D"
  timestamp: '2014-07-29T15:05:53Z'
title: Keyboard_CursorFlash
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x08CA\
**Function signature:** void Keyboard_CursorFlash(void)

The function of this syscall is not yet known in detail.

## Comments

Possibly related to
[Cursor_SetFlashOn]({{< ref "Syscalls/Cursor_SetFlashOn.md" >}}),
[Cursor_SetFlashOff]({{< ref "Syscalls/Cursor_SetFlashOff.md" >}}) and
[SetCursorFlashToggle]({{< ref "Syscalls/SetCursorFlashToggle.md" >}}).
