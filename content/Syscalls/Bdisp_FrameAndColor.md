---
revisions:
- author: ProgrammerNerd
  timestamp: '2015-02-22T23:45:52Z'
title: Bdisp_FrameAndColor
---

## Synopsis

**Header:** fxcg/display.h *(Not yet in
[libfxcg](https://github.com/Jonimoose/libfxcg))*\
**Syscall index:** 0x091E\
**Function signature:** unsigned short Bdisp_FrameAndColor(int
action,unsigned short flags);

## Parameters

**actions**

-   If action == 0 clear the internal flags.
-   If action == 1 Take action based upon the internal flags and then
    clear the internal flags.
-   If action == 2 Set the internal flags to 0x8000.
-   If action == 3 Or the value of flags (the parameter not the internal
    variable) with 0x8000 and set the internal variable.
-   Other values of action just cause the function to return see
    [#returns](#returns).

Possible flag values (this applies both to the parameter and the
internal variable flags):

-   bit 15 If set allows the function to carry out other actions as
    determined by other flags when action == 1
-   bit 4 If set enable 16-bit color mode, otherwise enable 8 color mode
-   bit 0 If set draw a black frame, otherwise draw a white frame.

## Returns

The final value of it's internal flags.

## Comments

Here is a disassembly of the function:

    Bdisp_FrameAndColor:
            sts.l   pr, @-r15
            tst r4, r4
            bt  clearFlags  ! Branch if R4 is zero
            mov r4, r0
            cmp/eq  #1, r0
            bt  R4One
            cmp/eq  #2, r0
            bt  R4Two
            cmp/eq  #3, r0
            bt  R4Three
            bra exit
            nop
    ! ---------------------------------------------------------------------------

    R4One:
            mov.l   #frameAndColorFlags, r1
            mov.w   #0xFFFF8000, r6
            extu.w  r6, r6      ! r6 = 32768
            mov.w   @r1, r0
            tst r6, r0      ! Is bit 15 clear?
            bt  exit        ! If so branch without a delay
            tst #16, r0
            mov.l   #Bdisp_SetDDRegisterB, r2
            bt/s    bit4Clear   ! Branch if bit 4 is clear
            mov #0, r4      ! If bit 4 is clear set r4 to 0
            mov #1, r4      ! Otherwise set it to one

    bit4Clear:
            jsr @r2     ! Bdisp_SetDDRegisterB
            nop
            mov.l   #frameAndColorFlags, r1
            mov.l   #DrawFrameC3, r2
            mov.w   @r1, r0
            tst #1, r0
            bt  whiteFrame  ! Branch if bit 0 is clear
            jsr @r2 ! DrawFrameC3
            mov #0, r4      ! Black frame
            bra clearFlags
            nop
    ! ---------------------------------------------------------------------------

    whiteFrame:
            jsr @r2 ! DrawFrameC3
            mov #7, r4

    clearFlags:
            mov.l   #frameAndColorFlags, r1
            mov #0, r2
            bra exit
            mov.w   r2, @r1
    ! ---------------------------------------------------------------------------

    R4Two:
            mov.l   #frameAndColorFlags, r1
            mov.w   #0xFFFF8000, r6
            bra exit
            mov.w   r6, @r1
    ! ---------------------------------------------------------------------------

    R4Three:
            mov.l   #frameAndColorFlags, r1
            mov.w   #0xFFFF8000, r6
            or  r6, r5
            mov.w   r5, @r1

    exit:
            mov.l   #frameAndColorFlags, r1
            lds.l   @r15+, pr
            rts
            mov.w   @r1, r0
    ! End of function Bdisp_FrameAndColor
