---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = SetBatteryType \\|\nheader\
    \ = fxcg/system.h \\| index = 0x12D4 \\| signature = void\nSetBatteryType(int\
    \ type) \\| synopsis = Sets the battery type used\nfor estimating\u2026\u201D"
  timestamp: '2014-08-01T16:39:24Z'
title: SetBatteryType
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x12D4\
**Function signature:** void SetBatteryType(int type)

Sets the battery type used for estimating the battery level. This is the
setting adjusted on start-up, or when one goes into System-\>F6-\>F1.

## Parameters

-   **type** - 1 for Alkaline batteries, 2 for Ni-MH batteries.
