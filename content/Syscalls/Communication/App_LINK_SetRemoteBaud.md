---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T14:29:54Z'
title: App_LINK_SetRemoteBaud
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1397\
**Function signature:** int App_LINK_SetRemoteBaud(void)

Changes the current communication settings in both the local and
connected device to 115200 baud, no parity and one stop bit.

## Returns

The timeout in milliseconds for data reception by the Link app.

## Comments

This syscall does the following:

-   Sends a *Protocol 7.00* packet to the connected device, with type 1
    (Command) and subtype "02" (Set link settings), and the 115200 baud,
    no parity and 1 stop bit settings encoded in the packet's data
    fields (see fxReverse for more information);
-   Closes the serial port (the connected device should do the same, or
    perhaps adjusts settings on-the-fly with
    [Serial_Open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}));
-   Opens the serial port with 115200 baud, no parity, 1 stop bit just
    like the remote calculator was requested to do.

During a normal, OS initiated communication, this procedure is performed
after reading the remote calculator's model with
[App_LINK_GetDeviceInfo]({{< ref "Syscalls/Communication/App_LINK_GetDeviceInfo.md" >}})
to determine if the high-speed 115200 bps mode is supported.

For more information on *Protocol 7.00*, see
[fxReverse.pdf](http://tny.im/dl/fxReverse2x.pdf).
