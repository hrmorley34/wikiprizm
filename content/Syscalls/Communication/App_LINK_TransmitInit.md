---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = App_LINK_TransmitInit\n\\\
    | header = fxcg/serial.h \\| index = 0x13A6 \\| signature = int\nApp_LINK_TransmitInit(struct\
    \ TransmitBuffer\\* txb) \\| synopsis =\nSends the t\u2026\u201D"
  timestamp: '2014-08-01T15:27:37Z'
title: App_LINK_TransmitInit
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x13A6\
**Function signature:** `int App_LINK_TransmitInit(struct TransmitBuffer* txb)`

Sends the transmit start packet for a file transfer using *Protocol
7.00*, and handles the file overwrite dialog in case the file already
exists in the connected device. Before calling this syscall, preparation
is necessary.

## Parameters

-   **txb** - pointer to a struct like this (comments relative to the
    transfer of a file in storage memory):

    ```c
    struct TTransmitBuffer {
        char device[0x008]; // fls0
        char directoryname[0x10A]; // can be an empty string, possibly controls in which folder the file goes on the destination device.
    // 0x112
        char fname1[0x10A]; // file name for the file on the destination device.
        char dummy[0x28];
        char fname2[0x00E];
    // 0x252
        unsigned short filename[0x10A]; // 16-bit string containing the full file name in the origin calculator, like this: \\fls0\ABC.G1A
    // 0x466
        char dummy2[0x1A];
    // 0x480
        int filesize; // the size, in bytes, of the file to send.
    // 0x484
        short dummy3;
    // 0x486
        char command; // set to 0x45
        char subcommand;
    // 0x488
        char datatype; // set to 0x80
        char dummy4_3;
        char dummy4_4;
        char dummy4_5;
    // 0x48C
        int handle; // initialize to -1
    // 0x490
        char dummy5[8];
    // 0x498
        char source; // set to 1
        char dummy6_1;
        char dummy6_2;
        char dummy6_3;
    // 0x49C
        int zero; // set to zero
    }
    ```

## Returns

0 on success, other values on error.

## Comments

After calling this syscall,
[App_LINK_Transmit]({{< ref "Syscalls/Communication/App_LINK_Transmit.md" >}})
can be used to proceed with the file transfer.

For more information on *Protocol 7.00*, see
[fxReverse.pdf](http://tny.im/dl/fxReverse2x.pdf).
