---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name =\nApp_LINK_SetReceiveTimeout_ms\
    \ \\| header = fxcg/serial.h \\| index =\n0x1409 \\| signature = void App_LINK_SetReceiveTimeout_ms(int\n\
    timeout) \\| synopsis = Sets the\u2026\u201D"
  timestamp: '2014-08-01T12:48:30Z'
title: App_LINK_SetReceiveTimeout_ms
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1409\
**Function signature:** void App_LINK_SetReceiveTimeout_ms(int timeout)

Sets the timeout for data reception by the built-in app Link.

## Parameters

-   **timeout** - timeout, in milliseconds, for data reception by the
    Link app.
