---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = App_LINK_GetDeviceInfo\n\\\
    | header = fxcg/serial.h \\| index = 0x1399 \\| signature = int\nApp_LINK_GetDeviceInfo(unsigned\
    \ int\\* calcType, unsigned short\\*\nosVer) \\| s\u2026\u201D"
  timestamp: '2014-08-01T15:08:45Z'
title: App_LINK_GetDeviceInfo
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1399\
**Function signature:** int App_LINK_GetDeviceInfo(unsigned int\*
calcType, unsigned short\* osVer)

Gets information about the currently connected remote device using
*Protocol 7.00*.

## Parameters

-   **calcType** - pointer to integer that will receive a value
    identifying the calculator type. Known values:
    -   1: Gy363 (fx-9860G SD or similar, probably represents all
        devices with USB connectivity and storage memory),
    -   2: Gy362 (fx-9750GII or similar, probably represents all devices
        without storage memory),
    -   3: Gy490 (fx-7400GII or similar, probably represents all devices
        without USB connectivity).
-   **osVer** - pointer to `unsigned short` that will receive the major
    and minor OS version of the connected device, as decimal (2.02 -\>
    202 == 0x00CA).

## Returns

0 on success, other values on error.

## Comments

For more information on *Protocol 7.00*, see
[fxReverse.pdf](http://tny.im/dl/fxReverse2x.pdf).
