---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = App_LINK_Transmit \\|\nheader\
    \ = fxcg/serial.h \\| index = 0x13A7 \\| signature = int\nApp_LINK_Transmit(struct\
    \ TransmitBuffer\\* txb) \\| synopsis = Opens\nand sends the\u2026\u201D"
  timestamp: '2014-08-01T15:34:25Z'
title: App_LINK_Transmit
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x13A7\
**Function signature:** int App_LINK_Transmit(struct TransmitBuffer\*
txb)

Opens and sends the source file in a file transfer using *Protocol
7.00*.
[App_LINK_TransmitInit]({{< ref "Syscalls/Communication/App_LINK_TransmitInit.md" >}})
must have been called before.

## Parameters

-   **txb** - pointer to a struct documented on
    [App_LINK_TransmitInit]({{< ref "Syscalls/Communication/App_LINK_TransmitInit.md" >}}).

## Returns

0 on success, other values on error.

## Comments

After transferring the file, you can use
[Comm_Terminate]({{< ref "Syscalls/Communication/Comm_Terminate.md" >}})
to put the connected device out of receive mode.

For more information on *Protocol 7.00*, see
[fxReverse.pdf](http://tny.im/dl/fxReverse2x.pdf).
