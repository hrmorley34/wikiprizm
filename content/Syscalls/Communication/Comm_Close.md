---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Comm_Close \\| header =\n\
    fxcg/serial.h \\| index = 0x1354 \\| signature = int Comm_Close(int\nmode) \\\
    | synopsis = Resets all communication buffers and closes the\ns\u2026\u201D"
  timestamp: '2014-08-01T13:00:36Z'
title: Comm_Close
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1354\
**Function signature:** int Comm_Close(int mode)

Resets all communication buffers and closes the
[serial]({{< ref "Technical_Documentation/serial.md" >}}) or
[USB]({{< ref "USB_Communication.md" >}}) communication channel,
depending on what is open.

## Parameters

-   **mode** - if 1, the channel is closed immediately without waiting
    for pending transmissions; otherwise, the function will not close
    the communication channel if there are pending transmissions.

## Returns

-   0 if successful;
-   5 if there are pending transmissions (only when **mode** is not 1).
