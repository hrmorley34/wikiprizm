---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Comm_Open \\| header =\n\
    fxcg/serial.h \\| index = 0x1353 \\| signature = int\nComm_Open(unsigned short\
    \ opt) \\| synopsis = Opens the \\[\\[serial\\]\\]\nor USB_Communication\u2026\
    \u201D"
  timestamp: '2014-08-01T12:55:50Z'
title: Comm_Open
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1353\
**Function signature:** int Comm_Open(unsigned short opt)

Opens the [serial]({{< ref "Technical_Documentation/serial.md" >}}) or
[USB]({{< ref "USB_Communication.md" >}}) communication channel.

## Parameters

-   **opt** - if the result of **`opt`**` & 0xFFEF` (bitwise AND) is 0x20,
    the USB communication channel is opened, otherwise
    [Serial_Open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}) will be
    called with **opt** as parameter, opening the serial channel.

## Returns

1 if USB communication was opened, 0 if serial.

