---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = SMEM_MapIconToExt \\|\nindex\
    \ = 0x0C2C \\| signature = int SMEM_MapIconToExt(unsigned char\\*\nfilename, unsigned\
    \ short\\* foldername, unsigned int\\* msgno,\nunsigned sh\u2026\u201D"
  timestamp: '2014-07-29T20:39:28Z'
title: SMEM_MapIconToExt
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0C2C\
**Function signature:** int SMEM_MapIconToExt(unsigned char\* filename,
unsigned short\* foldername, unsigned int\* msgno, unsigned short\*
iconbuffer)

Gets the icon and message number, corresponding to type name, for a
certain file or folder.

## Parameters

-   **filename** - a pointer to the filename of file for which to
    retrieve the icon.
-   **foldername** - a pointer to a 16 bit character array. If the array
    is empty, **filename** is analyzed to retrieve the icon for it.
    Otherwise, the folder icon is returned.
-   **msgno** - a pointer to an *int* that will hold the message number
    (suitable for use with
    [LocalizeMessage1]({{< ref "Syscalls/Locale/LocalizeMessage1.md" >}})
    or [PrintXY_2]({{< ref "Syscalls/Locale/PrintXY_2.md" >}}))
    corresponding to the file type name.
-   **iconbuffer** - pointer to a buffer that will receive an 0x12 x
    0x18, 16 bit (RGB565) color icon (0x1B0 short numbers). This icon
    has a magenta (0xF81F) background to be used as transparency mask.
    The bitmap can be displayed with
    [Bdisp_WriteGraphVRAM]({{< ref "Syscalls/Bdisp_WriteGraphVRAM.md" >}})
    or the custom routine [CopySpriteMasked](CopySpriteMasked).
