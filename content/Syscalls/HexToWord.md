---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = HexToWord \\| header =\n\
    fxcg/misc.h \\| index = 0x1345 \\| signature = void HexToWord(unsigned\nchar\\\
    * value, unsigned short\\* result) \\| synopsis = Converts the\nhexad\u2026\u201D"
  timestamp: '2014-07-31T17:23:53Z'
title: HexToWord
---

## Synopsis

**Header:** fxcg/misc.h\
**Syscall index:** 0x1345\
**Function signature:** void HexToWord(unsigned char\* value, unsigned
short\* result)

Converts the hexadecimal representation of a `unsigned short` to a
`unsigned short`.

## Parameters

-   **value** - pointer to a string containing the hexadecimal
    representation.
-   **result** - pointer to variable that will receive the conversion
    result.
