---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = NormIcon \\| index =\n0x1D8D\
    \ \\| signature = void NormIcon(unsigned int) \\| header =\nfxcg/display.h \\\
    | parameters = The meaning of the parameter is\nunknown. \\| syno\u2026\u201D"
  timestamp: '2014-07-29T19:27:04Z'
title: NormIcon
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D8D\
**Function signature:** void NormIcon(unsigned int)

The exact function of this syscall is unknown; it is probably related to
status area items.

## Parameters

The meaning of the parameter is unknown.

## Comments

Instead of trying to control individual status area items directly,
using [status area
flags]({{< ref "Syscalls/DefineStatusAreaFlags.md" >}}) may be more
appropriate.
