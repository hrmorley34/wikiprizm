---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = FrameColor \\| index =\n\
    0x02A3 \\| signature = unsigned short FrameColor(int mode, unsigned\nshort color)\
    \ \\| header = fxcg/display.h \\| parameters = \\*\n\u2019\u2018\u2019mode\u2019\
    \u2019\u2019 -\u2026\u201D"
  timestamp: '2014-07-29T10:43:12Z'
title: FrameColor
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x02A3\
**Function signature:** unsigned short FrameColor(int mode, unsigned
short color)

Sets the word at 0xFD801460, which controls the system-wide frame
color.

## Parameters

-   **mode** - If 0, the word at 0xFD801460 is set to 0xFFFF. If 1, the
    word at 0xFD801460 is set to **color**.
-   **color** - The color to which the frame is set when **mode** is 1.

## Returns

The value of word at 0xFD801460.
