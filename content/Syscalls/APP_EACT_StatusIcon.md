---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = APP_EACT_StatusIcon \\|\n\
    index = 0x0A87 \\| signature = void APP_EACT_StatusIcon(int) \\|\nheader = fxcg/display.h\
    \ \\| parameters = The meaning of the parameter\nis un\u2026\u201D"
  timestamp: '2014-07-29T19:12:52Z'
title: APP_EACT_StatusIcon
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0A87\
**Function signature:** void APP_EACT_StatusIcon(int)

The exact function of this syscall is unknown; it is probably related to
status area items.

## Parameters

The meaning of the parameter is unknown.

## Comments

Instead of trying to control individual status area items directly,
using [status area
flags]({{< ref "Syscalls/DefineStatusAreaFlags.md" >}}) may be more
appropriate.
