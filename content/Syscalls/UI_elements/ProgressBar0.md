---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:07:39Z'
title: ProgressBar0
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x180B\
**Function signature:** void ProgressBar0(int, int, int, int current,
int max)

Displays a progress bar on a message box.

## Parameters

The meaning of the first three parameters is still unknown, these should
be set to zero.

*int* **current** - current progress value.

*int* **max** - maximum value **current** is expected to take.

## Comments

Should be called with **current** set to zero, for initialization (this
syscall calls
[MsgBoxPush]({{< ref "Syscalls/UI_elements/MsgBoxPush.md" >}})). To
close the progress bar's message box use
[MsgBoxPop]({{< ref "Syscalls/UI_elements/MsgBoxPop.md" >}}).
[ProgressBar]({{< ref "Syscalls/UI_elements/ProgressBar.md" >}}) is a
simplified variant of this syscall.
