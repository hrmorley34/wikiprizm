---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:10:23Z'
title: StandardScrollbar
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0C5A\
**Function signature:** void StandardScrollbar(TStandardScrollbar\*
scrollbar)

Displays the standard scrollbar.

## Parameters

-   *TStandardScrollbar\** **scrollbar** - settings for the standard
    scrollbar:

`typedef unsigned short TStandardScrollbar[0x12]`

TStandardScrollbar\[0x10\] should be set to the maximum indicator range,
and TStandardScrollbar\[0x09\] to the current indicator position. The
indicator height is fixed - always six units of the maximum range,
presumably because on scrollbar screens, only six items are shown at a
time.

## Comments

[Scrollbar]({{< ref "Syscalls/UI_elements/Scrollbar.md" >}}) is a more
flexible variant of this syscall.
