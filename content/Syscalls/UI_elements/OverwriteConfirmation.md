---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:07:11Z'
title: OverwriteConfirmation
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x0D91\
**Function signature:** int OverwriteConfirmation(char\* name, int mode)

Shows a overwrite confirmation dialog containing a specified filename.

## Parameters

-   **name** - The file name to show.
-   **mode** - if zero, AC/On will not work as an exit key.

## Returns

0x7539 if the user accepts the overwrite by pressing F1, or other values
if the user cancels.
