---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:03:30Z'
title: DisplayMainMenu
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1E6A\
**Function signature:** void DisplayMainMenu(void)

Draws the Main Menu to VRAM, without actually switching to it. Does not
seem to be of much use to add-ins, but it is possible that this routine
is the one called by the Main Menu process every time it needs to be
redrawn.
