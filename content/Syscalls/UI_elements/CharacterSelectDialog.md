---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:02:28Z'
title: CharacterSelectDialog
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x07A2\
**Function signature:** short CharacterSelectDialog(void)

Opens the OS special character selection screen.

## Returns

The [multi-byte code]({{< ref "Multi-byte_strings.md" >}}) of the
selected character. Suitable for use with the
[DisplayMBString]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString.md" >}})
syscall and related ones, when performing text input.

## Comments

Some documentation says that syscall 0x111
([Bkey_ClrAllFlags]({{< ref "Syscalls/Keyboard/Bkey_ClrAllFlags.md" >}}))
has to be called before this one, to ensure the function keys work
correctly on this screen. In practice this has shown not to always be
necessary, especially if one does not set keyboard flags and maps to
anything other than the default, before calling this function.
