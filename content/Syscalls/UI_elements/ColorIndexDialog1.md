---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:02:37Z'
title: ColorIndexDialog1
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1815\
**Function signature:** unsigned char ColorIndexDialog1(unsigned char
initial_index, unsigned short disable_mask)

Opens the OS color selection dialog, which allows choosing one of eight
colors.

## Parameters

-   **initial_index** - the initially selected color;
-   **disable_mask** - what items to disable. The following applies to
    all ColorIndexDialog syscalls:
    -   0x8000: disables 9:Auto
    -   0x4000: disables A:Clear
    -   0x0001 to 0x0080: disables the corresponding color item

## Returns

The following return values apply to all ColorIndexDialog syscalls:

-   The selected color index, 0 to 7, when user selects a color from 1
    to 8.
-   0xFD when user selects 9:Auto.
-   0xFE when user selects A:Clear.
-   0xFF when user cancels using EXIT, QUIT or AC/ON.

## Comments

This version of the dialog doesn't allow for selecting "9: Auto" or "A:
Clear".

Key handling is done by the syscall, which returns when the user selects
a color or cancels using EXIT, QUIT or AC/ON.
