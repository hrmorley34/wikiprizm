---
revisions:
- author: Ahelper
  comment: "Did some research on timer ticks. I didn\u2019t get a large\ndataset to\
    \ confirm the ms to tick conversion"
  timestamp: '2014-11-23T04:28:02Z'
title: Timer_Install
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x08D9\
**Function signature:** int Timer_Install(int InternalTimerID, void
(\*handler)(void), int elapse)

Installs a [timer]({{< ref "/OS_Information/Timers.md" >}}), optionally at a specific
slot, which will run periodically at a given frequency.

## Parameters

-   **InternalTimerID** - slot to install the timer on. If 0, the system
    will automatically choose a free user timer slot - this is the
    recommended operation mode. For more information on the slots see
    [Timers]({{< ref "/OS_Information/Timers.md" >}}).
-   **handler** - pointer to function that will run periodically. This
    function should not take a long time to return, and must not call
    any direct-display (DD) syscall, as these are not reentrant.
-   **elapse** - time, in milliseconds, between each call to
    **handler**.

## Returns

The number of the slot where the timer is installed. This is the number
that should be used as parameter to other timer syscalls. If the
specified **InternalTimerID** is already installed, this function will
return -1.

## Comments

The timer is not started by default. Use
[Timer_Start]({{< ref "Syscalls/Timers/Timer_Start.md" >}}) to begin
running it.

Note that the actual precision of the timer is not in milliseconds, it
appears to run at 25Hz. Time from 0-31ms is 1 tick, the assuming every
40 milliseconds afterwards is an additional tick.
