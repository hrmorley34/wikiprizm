---
revisions:
- author: Gbl08ma
  timestamp: '2014-12-06T16:43:03Z'
title: Timer_Deinstall
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x08DA\
**Function signature:** int Timer_Deinstall(int InternalTimerID)

Uninstalls a [timer]({{< ref "/OS_Information/Timers.md" >}})
[installed]({{< ref "Syscalls/Timers/Timer_Install.md" >}}) at the given
slot.

## Parameters

-   **InternalTimerID** - slot of the timer to uninstall. The timer
    should be [stopped]({{< ref "Syscalls/Timers/Timer_Stop.md" >}})
    first.

## Returns

Returns 0 if the timer was uninstalled, -2 if **InternalTimerID** wasn't
installed.
