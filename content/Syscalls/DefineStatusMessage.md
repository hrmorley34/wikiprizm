---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T18:50:09Z'
title: DefineStatusMessage
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D77\
**Function signature:** void DefineStatusMessage(char\* msg, short P2,
char indexed_color, char P4)

Shows a message on the status area at the top of the screen.

## Parameters

-   **msg** - pointer to buffer with message to display.
-   **P2** - unknown meaning.
-   **indexed_color** - indexed color for the text
    ([PrintXY]({{< ref "Syscalls/PrintXY.md" >}}) colors).
-   **P4** - unknown meaning.
