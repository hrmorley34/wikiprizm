---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-12T13:56:06Z'
title: Bdisp_PutDisp_DD
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x025F\
**Function signature:** Bdisp_PutDisp_DD(void)

## Comments

Puts the VRAM on the screen. This function blocks until the copy of the
VRAM to the LCD through [DMA](DMAC) is finished.

It is possible to write a non-blocking implementation, see
[Non-blocking_DMA]({{< ref "Useful_Routines/Non-blocking_DMA.md" >}})
which is used in, for example, [ProgrammerNerd's MPEG
player](https://github.com/ComputerNerd/Casio-prizm-mpeg2-player).

Internally this function falls through to
[Bdisp_PutDisp_DD_stripe]({{< ref "Syscalls/Bdisp_PutDisp_DD_stripe.md" >}}).


    Bdisp_PutDisp_DD:                       ! CODE XREF: sub_8002CA70+B8�p
    ! sub_8003E16A+AF6�p ...
                    mov     #0xFFFFFFD7, r5
                    mov     #0, r4          ! y1=0
                    extu.b  r5, r5          ! y2=215
    ! End of function Bdisp_PutDisp_DD


    ! =============== S U B R O U T I N E =======================================


    Bdisp_PutDisp_DD_stripe:                ! CODE XREF: sub_800B458A+18C�p
