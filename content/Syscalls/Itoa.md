---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = itoa \\| header =\nfxcg/misc.h\
    \ \\| index = 0x1170 \\| signature = void itoa(int value,\nunsigned char\\* result)\
    \ \\| synopsis = Converts a signed integer to\nits base-10\u2026\u201D"
  timestamp: '2014-07-31T17:28:41Z'
title: Itoa
---

## Synopsis

**Header:** fxcg/misc.h\
**Syscall index:** 0x1170\
**Function signature:** void itoa(int value, unsigned char\* result)

Converts a signed integer to its base-10 representation.

## Parameters

-   **value** - integer to convert;
-   **result** - pointer to string that will hold the conversion result.
