---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = DefineStatusGlyph \\|\nindex\
    \ = 0x1D7B \\| signature = void DefineStatusGlyph(int idx, struct\nglyph_def\\\
    * glyph) \\| header = fxcg/display.h \\| parameters = \\*\n\u2018\u2019int\u2019\
    \u2019\u2026\u201D"
  timestamp: '2014-07-29T18:45:59Z'
title: DefineStatusGlyph
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D7B\
**Function signature:** `void DefineStatusGlyph(int idx, struct glyph_def* glyph)`

Defines a custom glyph to be shown on the status area.

## Parameters

-   *int* **idx** - index of the glyph in the array.
-   *struct glyph_def\** **glyph** - pointer to a struct containing
    information on the glyph to display:

    ```c
    struct glyph_def {
        short    dx;    // usually 0x24, width of the glyph
        short    dy;    // usually 0x16, height of the glyph 
        void*    shortcolor_bitmap; // 2-byte-per-pixel (RGB565) bitmap
        short    xalignment; // alignment of the glyph in the status bar, see below.
        short    xoffset; // offset of the glyph, meaning varies with xalignment.
    }
    ```

-   If xalignment is 1, the glyph is positioned left of the rightmost
    enabled icon. xoffset is added to the x coordinate.
-   If xalignment is 2, the glyph is positioned right justified. xoffset
    is subtracted (x=384-dx-xoffset).
-   If xalignment is 3, the glyph is positioned to xoffset (x=xoffset).
