---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = SetBacklightDuration\n\\\
    | header = fxcg/system.h \\| index = 0x12D8 \\| signature = void\nSetBacklightDuration(char\
    \ durationInHalfMinutes) \\| synopsis = Sets\nthe amo\u2026\u201D"
  timestamp: '2014-08-01T16:28:14Z'
title: SetBacklightDuration
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x12D8\
**Function signature:** void SetBacklightDuration(char
durationInHalfMinutes)

Sets the amount of time after which the backlight dims if no key is
pressed.

## Parameters

-   **durationInHalfMinutes** - amount of half minutes (30 second units)
    the calculator should wait after the last key press, before
    automatically reducing the brightness to the lowest level one can
    set on the System menu. Minimum value is 1 (30 seconds), no maximum
    is known other than the limits of `char`.
