---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Alpha_SetData \\|\nheader\
    \ = fxcg/system.h \\| index = 0x0035 \\| signature = void\nAlpha_SetData(char\
    \ VarName, struct BCDvalue\\* Src) \\| synopsis =\nSets the value of\u2026\u201D"
  timestamp: '2014-08-03T22:41:08Z'
title: Alpha_SetData
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x0035\
**Function signature:** void Alpha_SetData(char VarName, struct
BCDvalue\* Src)

Sets the value of a Alpha variable, which can be used in most expression
evaluators throughout the OS, as well as BASIC programs.

## Parameters

-   **VarName** - name of the variable to change.
-   **Src** - pointer to a struct used for number representation by the
    OS math library, and that is documented at
    [BCDtoInternal]({{< ref "Syscalls/BCDtoInternal.md" >}}) (the
    syscall used in aiding the conversion from a `double` to a
    BCDvalue).
