---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:26:39Z'
title: APP_SYSTEM
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1E13\
**Function signature:** void APP_SYSTEM(void);

Opens the built-in "System" app.

## Comments

On the "fx-CG10/20 Manager" emulator, the behavior is no different from
the usual one, meaning that "unavailable" options remain locked.

Note that even though most built-in apps do not return, the calling code
is kept on stack and one can return to it using a ugly hack, for example
through [timers]({{< ref "/OS_Information/Timers.md" >}}), setjmp and longjmp. The
reason why they don't return is that they expect to use [GetKey as an
exit point]({{< ref "Syscalls/Keyboard/GetKey.md" >}}).
