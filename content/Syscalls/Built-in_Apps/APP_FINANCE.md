---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:22:58Z'
title: APP_FINANCE
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x0C6B\
**Function signature:** void APP_FINANCE(int, int)

Opens the built-in Finance app.

## Parameters

The meaning of the two parameters is unknown; they are probably the same
as, or similar to, the isAppli and OptionNum options of the main
function template from the fx-9860G SDK.

## Comments

Note that even though most built-in apps do not return, the calling code
is kept on stack and one can return to it using a ugly hack, for example
through [timers]({{< ref "/OS_Information/Timers.md" >}}), setjmp and longjmp. The
reason why they don't return is that they expect to use [GetKey as an
exit point]({{< ref "Syscalls/Keyboard/GetKey.md" >}}).
