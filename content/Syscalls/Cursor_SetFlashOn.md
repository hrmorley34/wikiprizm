---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Cursor_SetFlashOn \\|\nindex\
    \ = 0x08C7 \\| signature = void Cursor_SetFlashOn(unsigned char\ncursor_type)\
    \ \\| header = fxcg/display.h \\| parameters = \\*\n\u2019\u2019\u2019cursor_type\u2026\
    \u201D"
  timestamp: '2014-07-29T14:59:09Z'
title: Cursor_SetFlashOn
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x08C7\
**Function signature:** void Cursor_SetFlashOn(unsigned char
cursor_type)

Enables the cursor display at the current cursor position, set by
[locate_OS]({{< ref "Syscalls/locate_OS.md" >}}).

## Parameters

-   **cursor_type** - cursor type, which is stored to FlashStyle by
    syscall 0x01F2 according to the map below:

| cursor_type | FlashStyle |
|-------------|------------|
| 0           | 0          |
| 1           | 0          |
| 2           | 0          |
| 3           | 0          |
| 4           | 0          |
| 5           | 8          |
| 6           | 0x10       |
| 7           | 0x11       |
| 8           | 0          |
| 9           | 0x12       |
| 10          | 0x14       |
| 11          | 0x18       |
| 12          | 0x40       |
| else        | 0          |
|             |            |

Whenever the result of the previous map is zero, the result is not
stored to FlashStyle by the aforementioned syscall.
