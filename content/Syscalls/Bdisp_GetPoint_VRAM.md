---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:59:27Z'
title: Bdisp_GetPoint_VRAM
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0267\
**Function signature:** unsigned short Bdisp_GetPoint_VRAM(int x, int y)

Gets a single point (x, y) in the Video RAM (VRAM).

## Parameters

-   **x**: pixel column, 0 to 383.
-   **y** - pixel row, 0 to 215.

## Returns

The value of the specified
[pixel]({{< ref "Technical_Documentation/Display.md" >}}).

## Comments

It is strongly recommended that you use the VRAM-modifying functions
from the [Useful_Routines]({{< ref "Useful_Routines.md" >}}) page
instead. Also refer to the [display
concepts]({{< ref "Technical_Documentation/Display.md" >}}) article.
