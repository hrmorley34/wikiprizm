---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Serial_Write \\| header\n\
    = fxcg/serial.h \\| index = 0x1BBE \\| signature = int\nSerial_Write(const unsigned\
    \ char\\* buf, int count) \\| synopsis =\nSends a number of by\u2026\u201D"
  timestamp: '2014-08-01T10:45:20Z'
title: Serial_Write
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BBE\
**Function signature:** int Serial_Write(const unsigned char\* buf, int
count)

Sends a number of bytes through the 3-pin serial port, by putting them
in the transmit buffer.

## Parameters

-   **buf** - pointer to buffer containing the bytes to transmit;
-   **count** - amount of bytes to transmit from **buf**.

## Returns

-   0 if successful,
-   2 if no space is available in the serial transmit buffer (which has
    a size of 256 bytes),
-   3 if the serial channel is not
    [open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}).
