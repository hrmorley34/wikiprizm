---
revisions:
- author: Dr-carlos
  comment: Serial_Peek is included in libfxcg
  timestamp: '2022-03-26T05:37:32Z'
title: Serial_Peek
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BBB\
**Function signature:** int Serial_Peek(int idx, unsigned char\* out)

Reads, without removing, a single byte from a specified position in the
serial input buffer.

## Parameters

-   **idx** - index in the input buffer of the byte to read;
-   **out** - pointer to `unsigned char` that will receive the fetched
    byte.

## Returns

-   0 if successful,
-   1 if the required byte is not available (there are less bytes in the
    buffer than **idx**),
-   3 if the serial channel is not
    [open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}).
