---
revisions:
- author: Dr-carlos
  comment: Serial_ClearTX is included in libfxcg
  timestamp: '2022-03-26T05:39:01Z'
title: Serial_ClearTX
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BC2\
**Function signature:** int Serial_ClearTX(void)

Clears the serial transmit buffer.

## Returns

Always 0.
