---
revisions:
- author: Dr-carlos
  comment: Serial_ClearRX is included in libfxcg
  timestamp: '2022-03-26T05:37:53Z'
title: Serial_ClearRX
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BC1\
**Function signature:** int Serial_ClearRX(void)

Clears the serial receive buffer.

## Returns

-   0 if successful,
-   3 if the serial channel is not
    [open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}).
