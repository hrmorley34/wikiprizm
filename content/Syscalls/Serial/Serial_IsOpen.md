---
revisions:
- author: Tari
  timestamp: '2012-05-30T21:23:43Z'
title: Serial_IsOpen
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BC6\
**Function signature:** int Serial_IsOpen(void)

Checks whether the serial port is ready to use.

## Returns

1 if the serial port is currently open, or 3 otherwise.

## Comments

Use [Serial_Open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}) to open
the port, and
[Serial_Close]({{< ref "Syscalls/Serial/Serial_Close.md" >}}) to close
it.
