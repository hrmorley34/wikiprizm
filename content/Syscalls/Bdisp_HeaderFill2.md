---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T19:03:15Z'
title: Bdisp_HeaderFill2
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D87\
**Function signature:** void Bdisp_HeaderFill2( unsigned int col_start,
unsigned int col_count, unsigned char c_idx1, unsigned char c_idx2 )

Same as [Bdisp_HeaderFill]({{< ref "Syscalls/Bdisp_HeaderFill.md" >}}),
but lets you select in which columns the checkers pattern will appear?
--[Gbl08ma](User:Gbl08ma) ([talk](User_talk:Gbl08ma)) 10:39, 9 August
2012 (EDT)

## Comments

See color table on
[Bdisp_HeaderFill]({{< ref "Syscalls/Bdisp_HeaderFill.md" >}}).
