---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = ByteToHex \\| header =\n\
    fxcg/misc.h \\| index = 0x1347 \\| signature = void ByteToHex(unsigned\nchar value,\
    \ unsigned char\\* result) \\| synopsis = Converts a byte to\ni\u2026\u201D"
  timestamp: '2014-07-31T17:06:17Z'
title: ByteToHex
---

## Synopsis

**Header:** fxcg/misc.h\
**Syscall index:** 0x1347\
**Function signature:** void ByteToHex(unsigned char value, unsigned
char\* result)

Converts a byte to its hexadecimal representation.

## Parameters

-   **value** - the byte to convert.
-   **result** - pointer to buffer that will receive the result.
