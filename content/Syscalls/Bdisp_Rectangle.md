---
revisions:
- author: Dr-carlos
  comment: Add Bdisp_Rectangle Syscall
  timestamp: '2022-03-26T05:03:58Z'
title: Bdisp_Rectangle
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0924
**Function signature:** void Bdisp_Rectangle(int x1, int y1, int x2, int y2, char color)

Draws a rectangle to VRAM, avoiding the status area.

## Parameters

-   **x1** - horizontal coordinate of the top-left corner of the
    rectangle, 0 to 383.
-   **y1** - vertical coordinate of the top-left corner of the
    rectangle, 0 to 191. 24 pixels are automatically added to bypass the
    status area.
-   **x2** - horizontal coordinate of the bottom-right corner of the
    rectangle, 0 to 383.
-   **y2** - vertical coordinate of the bottom-right corner of the
    rectangle, 0 to 191. 24 pixels are automatically added to bypass the
    status area.
-   **color** - Line color of the rectangle, as listed in note 3 of
    [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}).
