---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name =\nGetGetkeyToMainFunctionReturnFlag\
    \ \\| header = fxcg/keyboard.h \\|\nindex = 0x1E99 \\| signature = int\nGetGetkeyToMainFunctionReturnFlag(void)\
    \ \\| synopsis = Gets th\u2026\u201D"
  timestamp: '2014-07-31T16:21:02Z'
title: GetGetkeyToMainFunctionReturnFlag
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x1E99\
**Function signature:** int GetGetkeyToMainFunctionReturnFlag(void)

Gets the current behavior of
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) with regards to
access to the Main Menu.

## Returns

0 if access to the main menu is blocked, 1 if not.

## Comments

[EnableGetkeyToMainFunctionReturn]({{< ref "EnableGetkeyToMainFunctionReturn.md" >}})
can be used to enable this (set to 1), and
[DisableGetkeyToMainFunctionReturn]({{< ref "DisableGetkeyToMainFunctionReturn.md" >}})
disables it (set to 0).
