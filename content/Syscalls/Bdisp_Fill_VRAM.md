---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T11:02:15Z'
title: Bdisp_Fill_VRAM
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0275\
**Function signature:** void Bdisp_Fill_VRAM(int color, int mode)

Fills different areas of VRAM with specified color, depending on
**mode**.

## Parameters

-   *int* **color** - color to fill the VRAM with
-   *int* **mode** - area of the VRAM to fill. When **mode** is 1, the
    area below the status area is filled; when **mode** is 2, the area
    between the status area and the F-key labels is filled; and when
    **mode** is 3 or 4, the whole VRAM is filled.
