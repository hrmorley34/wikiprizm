---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\|name = Bfile_GetFileSize_OS\n\\|header\
    \ = fxcg/file.h \\|index = 0x1DA6 \\|signature = int\nBfile_GetFileSize_OS(int\
    \ handle) \\|synopsis = Gets the file size of\na open file, g\u2026\u201D"
  timestamp: '2014-07-30T10:47:16Z'
title: Bfile_GetFileSize_OS
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DA6\
**Function signature:** int Bfile_GetFileSize_OS(int handle)

Gets the file size of a open file, given its handle.

## Parameters

-   **handle** - the handle of the file one wants to know the size (this
    is the value returned by
    [Bfile_OpenFile_OS]({{< ref "Syscalls/Bfile/Bfile_OpenFile_OS.md" >}}))

## Returns

Returns the file size.

## Comments

If you are trying to get the file size of multiple files at once without
having to [open]({{< ref "Syscalls/Bfile/Bfile_OpenFile_OS.md" >}}) and
[close]({{< ref "Syscalls/Bfile/Bfile_CloseFile_OS.md" >}}) each, using
the file search functions
([Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}}) and
friends) may be easier and faster, because with these the file size is
returned in the the file_type_t struct, and one only needs to open and
close the find handle.

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.
