---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T11:02:34Z'
title: Bfile_CloseFile_OS
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DA4\
**Function signature:** int Bfile_CloseFile_OS(int handle)

Closes an open file, given its handle.

## Parameters

-   **handle** - the handle of the file to close (this is the value
    returned by
    [Bfile_OpenFile_OS]({{< ref "Syscalls/Bfile/Bfile_OpenFile_OS.md" >}}))

## Comments

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.
