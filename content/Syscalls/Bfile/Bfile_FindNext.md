---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T10:38:23Z'
title: Bfile_FindNext
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DB9\
**Function signature:** int Bfile_FindNext(int FindHandle, const
unsigned short\* foundfile, file_type_t\* fileinfo);

This function is used to list files and folders based on a find handle
created by
[Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}}).

## Parameters

-   *int* **FindHandle** - The find handle created by
    [Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}}).
-   *const unsigned short\** **foundfile** - The string used to store
    the found file or folder name.
-   *file_type_t* **fileinfo** - Pointer to a file_type_t struct. See
    [Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}})
    for the struct definition.

## Returns

-   0 - Successful read. Data is stored to **foundfile** and
    **fileinfo**
-   -16 - There are no more files or folders to be read. When this
    happens, call
    [Bfile_FindClose]({{< ref "Syscalls/Bfile/Bfile_FindClose.md" >}})
    on the **FindHandle**.

## Comments

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.

## Example

To read files, create a handle with
[Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}}). If
the return value is 0, then the foundfile and fileinfo values are set.
Keep calling this function, checking the return, then printing if 0 to
read all files in the find handle. See [the Using the File System
tutorial]({{< ref "Tutorials/Using_the_File_System.md" >}}) for a full
example
