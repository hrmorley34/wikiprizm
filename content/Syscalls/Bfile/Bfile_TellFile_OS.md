---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T10:59:21Z'
title: Bfile_TellFile_OS
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DAB\
**Function signature:** int Bfile_TellFile_OS(int handle)

Gets the current position of the file handler.

## Parameters

-   **handle**: File handle as returned by
    [Bfile_OpenFile_OS]({{< ref "Syscalls/Bfile/Bfile_OpenFile_OS.md" >}}).

## Returns

Returns the current position in the file handle.

## Comments

Use
[Bfile_SeekFile_OS]({{< ref "Syscalls/Bfile/Bfile_SeekFile_OS.md" >}})
to set the current file handle position.

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.
