---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T23:39:07Z'
title: Bfile_FindClose
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DBA\
**Function signature:** int Bfile_FindClose( int FindHandle );

This function closes a find handle when you are done with it.

## Parameters

-   *int* **FindHandle** - Handle created by
    [Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}}).

## Returns

0 on success, or a negative error code on failure.

## Comments

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.
