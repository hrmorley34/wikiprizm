---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = GetAppName \\| index =\n\
    0x1E9F \\| signature = unsigned char\\* GetAppName(unsigned char\\*\nname); \\\
    | header = fxcg/app.h \\| synopsis = Gets the internal name\nof the c\u2026\u201D"
  timestamp: '2014-07-29T09:56:53Z'
title: GetAppName
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1E9F\
**Function signature:** unsigned char\* GetAppName(unsigned char\*
name);

Gets the internal name of the currently running app.

## Parameters

-   **name** - A pointer to a buffer into which the app name will be
    copied.

## Returns

A pointer to the app name.

## Comments

The name in question is the one specified by the "-n internal:" option
to [mkg3a]({{< ref "mkg3a.md" >}}), but with a preceding @. So if one
uses the "-n internal:TEST" option with mkg3a, the result of this
syscall will be \@TEST.
