---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-04T12:04:45Z'
title: DisplayMBString
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x121D\
**Function signature:** void DisplayMBString(unsigned char \*buffer, int
start, int cursor, int x, int y)

Draws a [multi-byte string]({{< ref "Multi-byte_strings.md" >}}) for
manual single-line text editing. A blinking cursor is provided and the
string does not wrap.

## Parameters

-   **buffer** - A multi-byte (i.e., supports special characters) text
    string that will be displayed, can be constant.
-   **start** - Location of the first character to display. 0 is the
    beginning of the string, \>0 will shift the string to the left. If
    **`cursor`**`>`**`start`** then the location of the first character
    will be **`cursor`**.
-   **cursor** - Location of the cursor from the start of the string.
    This is absolute and is from the start of the displayed text
    (**cursor** of 3 and **start** of 2, the cursor is located after the
    first character displayed, not after the third). 0 is before the
    first character.
-   **x** - X location to draw the string in "homescreen" coordinates,
    similar to [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}).
-   **y** - Y location to draw the string in "homescreen" coordinates,
    similar to [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}).

## Comments

Function is non-blocking, meant for manual string entry. String editing
has to be done with other function(s) like the conjunction of
[EditMBStringCtrl]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl.md" >}})
and
[EditMBStringChar]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringChar.md" >}}).
Cursor blinking cannot be moved unless this function is re-called with
updated **cursor**.

Result of setting **cursor** out of bounds has not been tested, overflow
characters are not drawn.
