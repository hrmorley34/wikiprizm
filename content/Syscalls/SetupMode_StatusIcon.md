---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = SetupMode_StatusIcon\n\\\
    | index = 0x0A8B \\| signature = void SetupMode_StatusIcon(void) \\|\nheader =\
    \ fxcg/display.h \\| synopsis = The exact function of this\nsyscal\u2026\u201D"
  timestamp: '2014-07-29T19:14:35Z'
title: SetupMode_StatusIcon
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0A8B\
**Function signature:** void SetupMode_StatusIcon(void)

The exact function of this syscall is unknown; it is probably related to
status area items.

## Comments

Instead of trying to control individual status area items directly,
using [status area
flags]({{< ref "Syscalls/DefineStatusAreaFlags.md" >}}) may be more
appropriate.
