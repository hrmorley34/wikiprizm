---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T13:48:32Z'
title: MCS_DeleteDirectory
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1516\
**Function signature:** int MCS_DeleteDirectory(unsigned char\* dir)

Deletes a directory from main memory.

## Parameters

-   **dir** - The name of the directory to delete in letters, numbers,
    and symbols.

## Returns

-   0 for success;
-   0xF0 if **dir** is null or length of \*dir is 0;
-   0x40 if directory **dir** does not exist;
-   0x45 if the directory contains items (these must be deleted
    beforehand with
    [MCSDelVar2]({{< ref "Syscalls/MCS/MCSDelVar2.md" >}}));
-   0x46 if \*dir is "@APLWORK" or "main" (system folders which can't be
    deleted);
-   0x47 if certain flags for directory **dir** are set.

## Comments

Note that empty folders aren't seen by add-ins.
