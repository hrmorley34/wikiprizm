---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T13:47:13Z'
title: MCS_CreateDirectory
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x154B\
**Function signature:** int MCS_CreateDirectory(unsigned char\* dir)

Create the named directory in main memory.

## Parameters

-   **dir** - The name of the directory to create in letters, numbers,
    and symbols.

## Returns

-   0 for success;
-   0xF0 if **dir** is null or its length is zero;
-   0x42 if the directory already exists;
-   0x43 if the directory space is exhausted.

## Comments

Empty folders aren't seen by add-ins.
