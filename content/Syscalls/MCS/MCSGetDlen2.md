---
revisions:
- author: Gbl08ma
  timestamp: '2014-12-06T16:11:27Z'
title: MCSGetDlen2
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1562\
**Function signature:** int MCSGetDlen2(unsigned char\* dir, unsigned
char\* item, int\* data_len)

Gets the length in bytes of the item in main memory, and selects it for
posterior usage by, for example,
[MCSGetData1]({{< ref "Syscalls/MCS/MCSGetData1.md" >}}).

## Parameters

-   *unsigned char\** **dir** - The name of the directory where the item
    is located.
-   *unsigned char\** **item** - The name of the item.
-   *int\** **data_len** - Pointer to variable that will receive the
    length of the data.

## Returns

-   0 for success;
-   other values on failure.

## Comments

Selects **dir**\\**item** as the item for
[MCSGetData1]({{< ref "Syscalls/MCS/MCSGetData1.md" >}}) to operate
with. As explained on its article, process switching (i.e.
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}})) can't occur between
the calls to these syscalls.
