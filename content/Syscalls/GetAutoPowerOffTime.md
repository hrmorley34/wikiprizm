---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = GetAutoPowerOffTime \\|\n\
    header = fxcg/system.h \\| index = 0x1E91 \\| signature = int\nGetAutoPowerOffTime(void)\
    \ \\| synopsis = Gets the automatic power off\ntime. \\|\u2026\u201D"
  timestamp: '2014-08-01T16:02:16Z'
title: GetAutoPowerOffTime
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1E91\
**Function signature:** int GetAutoPowerOffTime(void)

Gets the automatic power off time.

## Returns

The number of minutes the calculator will wait, inactive, before
automatically powering off.
