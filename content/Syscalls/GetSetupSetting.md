---
revisions:
- author: ProgrammerNerd
  timestamp: '2015-02-15T02:00:30Z'
title: GetSetupSetting
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x0031\
**Function signature:** unsigned char GetSetupSetting(unsigned int
SystemParameterNo)

Gets a [Setup]({{< ref "Setup.md" >}}) setting.

## Parameters

-   **SystemParameterNo** - index of the setting to retrieve.

## Returns

The value of the setup setting specified on **SystemParameterNo**.
