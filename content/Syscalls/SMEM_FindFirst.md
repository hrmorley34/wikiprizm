---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = SMEM_FindFirst \\|\nheader\
    \ = fxcg/file.h \\| signature = int SMEM_FindFirst(const\nunsigned short\\* pattern,\
    \ unsigned short\\* foundfile) \\| index =\n0x0DAC \\| paramete\u2026\u201D"
  timestamp: '2014-07-30T13:03:50Z'
title: SMEM_FindFirst
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x0DAC\
**Function signature:** int SMEM_FindFirst(const unsigned short\*
pattern, unsigned short\* foundfile)

The exact function of this syscall is unknown; it is probably a
low-level version of
[Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}}).

## Parameters

The exact meaning of the parameters is unknown.

## Returns

The return values of the function are not yet known.
