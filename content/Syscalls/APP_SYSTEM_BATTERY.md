---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:04:31Z'
title: APP_SYSTEM_BATTERY
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1E07\
**Function signature:** void APP_SYSTEM_BATTERY(int opt);

Opens the "Battery setting" screen that shows in the start-up wizard and
in the System menu (when F6 then F1 is pressed).

## Parameters

-   **opt** - Controls whether the screen should behave like in the
    wizard (1), or like in the System menu (0).

## Comments

The setting for the battery type actually changes when an option is
selected, even on the "fx-CG10/20 Manager" emulator.

If the value of the parameter is zero, the syscall returns when the user
presses EXIT. If the value of the parameter is 1, it returns when the
user selects a battery type, accepts the warning message and presses F6.
