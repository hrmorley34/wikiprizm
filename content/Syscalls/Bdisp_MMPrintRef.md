---
revisions:
- author: Dr-carlos
  comment: Bdisp_MMPrintRef is included in libfxcg
  timestamp: '2022-03-26T05:42:24Z'
title: Bdisp_MMPrintRef
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0D08\
**Function signature:** void Bdisp_MMPrintRef(int\* x, int\* y, const
char\* string, int mode_flags, int xlimit, int P6, int P7, int color,
int back_color, int writeflag, int P11);

Draws a line of text on the VRAM with the [16px
Font]({{< ref "Fonts.md" >}}), used for app names in the Main Menu. Very
similar to [Bdisp_MMPrint]({{< ref "Syscalls/Bdisp_MMPrint.md" >}})
(which is a wrapper around this function), but the coordinates are
passed by reference and update as the text is drawn, which may be useful
in some situations.

## Parameters

-   *int\** **x** - Pointer to an integer containing the position of the
    text on a horizontal axis, in pixels. It is automatically updated to
    the position for the next character as the text is drawn.
-   *int\** **y** - Pointer to an integer containing the position of the
    text on a vertical axis, in pixels. Automatically incremented by 24
    pixels (status area) unless **mode_flags** is set to 0x40.
-   *const char\** **string** - The string to display. Certain
    [multi-byte characters]({{< ref "Multi-byte_strings.md" >}}) are
    supported.
-   *int* **mode_flags** - controls whether to clear text background, or
    invert background and foreground colors:
    -   0: normal text mode
    -   1: swaps **color** and **back_color** (the foreground color
        becomes **back_color**)
    -   2: transparent background mode; **back_color** is not used
    -   4: inverts colors (so red becomes cyan, blue becomes yellow,
        etc.)
    -   0x40: allows for drawing text on the status area
    -   One can combine at least some of these flags (with a binary
        AND). So if flag 1 and 2 are set, this function draws only the
        text background in **back_color**, leaving the foreground
        untouched (transparent)
-   *int* **xlimit** - Upper horizontal limit - text will be cut past
    this point. Set to 0xFFFFFFFF for it to be ignored.
-   *int* **P6** - Still unknown, set to 0
-   *int* **P7** - Still unknown, set to 0
-   *int* **color** - The color of the characters
-   *int* **back_color** - The color of the characters' background.
    Depends on **mode_flags**
-   *int* **writeflag** - Set to 1 for the text to be actually drawn;
    setting to 0 only makes the value of **x** update as if the text was
    being drawn.
-   *int* **P11** - Still unknown, set to 0
