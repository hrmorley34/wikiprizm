---
title: On-Chip Peripherals
bookCollapseSection: true
aliases:
  - /Peripherals/
---

The SH7305 is comprised of many different peripherals that surround a
SH4a core in the SH7305. Due to a lack of known documentation, the
peripherals included in the SH7305 are guessed based on similarities to
other SuperH CPUs with known documentation.

## Unknown Peripherals {#unknown_peripherals}

Please document all reads/writes to unknown peripherals. This is an
effort to produce documentation by analyzing their usage. Please report
the address, access size in bytes, mark read and/or write, and note what
the surrounding code was doing or any hypothesis about the function.
Note that 0x8xxxxxxx-0x9xxxxxxx and 0xAxxxxxxx-0xBxxxxxxx addresses are
identical, standardize on 0xAxxxxxxx-0xBxxxxxxx addresses.

| Address | Size | Read                                                          | Write                                                         | Notes                                                                                                                                                                                   |
|-----------|------|---------------------------------------------------------------|---------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| A400 0010 | 4    | :red_circle:  | :large_blue_circle: | Writes 4 on cold start. Later, reads it, then writes 0x808 and 0.                                                                                                                       |
| A400 001C | 4    | :large_blue_circle: | :large_blue_circle: | Writes 0x800 on cold start                                                                                                                                                              |
| A400 0024 | 4    | :large_blue_circle: | :red_circle:  |                                                                                                                                                                                         |
| A42F 0004 | 4    | :red_circle:  | :large_blue_circle: | Right after reading BSC, before reading/write to A400001C                                                                                                                               |
| A405 0184 | 2    | :red_circle:  | :large_blue_circle: | Writes 0x9554 on cold start. Order for next 3: ...86, ...88, ...8A, ...84. This overlaps with the SH7724's PFC's PULCR (pullup control register), but most likely not the same.         |
| A405 0186 | 2    | :red_circle:  | :large_blue_circle: | Writes 0x5555 on cold start. Overlaps with the SH7724's PFC                                                                                                                             |
| A405 0188 | 2    | :red_circle:  | :large_blue_circle: | Writes 0x5555 on cold start. Overlaps with the SH7724's PFC                                                                                                                             |
| A405 018A | 2    | :red_circle:  | :large_blue_circle: | Writes 0x5515 on cold start. Overlaps with the SH7724's PFC                                                                                                                             |
| A408 0008 | 2    | :large_blue_circle: | :large_blue_circle: | Reads and writes 0 back.                                                                                                                                                                |
| A408 008C | 1    | :red_circle:  | :large_blue_circle: | Writes 0xC                                                                                                                                                                              |
| A415 0014 | 4    | :large_blue_circle: | :large_blue_circle: | Writes 0x100. This occurs in a block of writing 0x100 to A415 003C and A415 0008 (CPG-related).                                                                                         |
| A44D 00D0 | 1    | :red_circle:  | :large_blue_circle: | Initialized to 0x01 in a large peripheral init block                                                                                                                                    |
| A44D 00D4 | 4    | :red_circle:  | :large_blue_circle: | Initialized to 0xFFFFFFFF in a large peripheral init block                                                                                                                              |
| A44D 00D8 | 4    | :red_circle:  | :large_blue_circle: | Initialized to 0xFFFFFFFF in a large peripheral init block                                                                                                                              |
| A44D 00DC | 1    | :red_circle:  | :large_blue_circle: | Initialized to 0x0 in a large peripheral init block                                                                                                                                     |
| FEC1 5040 | 2    | :large_blue_circle: | :red_circle:  | Reads on boot in a BSC-reading block. If this is the SH7730, then it read 2 bytes in a write-only 4-byte SDRAM mode register.                                                           |
| FE20 0000 | 4    | :large_blue_circle: | :large_blue_circle: | This is the base of a memory block from 0xFE200000 to 0xFE227FFF (160KB). Bootloader copies... something from high to lower, whole address range.                                       |
| FE24 0000 | 4    | :large_blue_circle: | :large_blue_circle: | This is the base of a memory block from 0xFE240000 to 0xFE269FFF (168KB). Bootloader copies... something from high to lower, whole address range.                                       |
| FE28 0000 | 4    | :large_blue_circle: | :large_blue_circle: | This is the base of a memory block from 0xFE280000 to 0xFE28BFFF (176KB). Bootloader copies... something from high to lower, whole address range.                                       |
| FE30 0000 | 4    | :large_blue_circle: | :large_blue_circle: | This is the base of another memory block from 0xFE300000 to 0xFE327FFF (160KB). Also has something copied high to low right after the 0xFE200000 copy.                                  |
| FE34 0000 | 4    | :large_blue_circle: | :large_blue_circle: | This is the base of another memory block from 0xFE340000 to 0xFE369FFF (168KB). Also has something copied high to low right after the 0xFE240000 copy.                                  |
| FE38 0000 | 4    | :large_blue_circle: | :large_blue_circle: | This is the base of another memory block from 0xFE380000 to 0xFE38BFFF (176KB). Also has something copied high to low right after the 0xFE280000 copy. (0xBC000 bytes/752KB total above |

## Peripherals List {#peripherals_list}

List of peripherals that match documentation

<table>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>SH Core</p></th>
<th><p>Base Address</p></th>
<th><p>Verified</p></th>
<th><p>Emulated</p></th>
<th><p>Notes</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="{{< ref "Bus_State_Controller.md" >}}">BSC</a></p></td>
<td><p>SH7730</p></td>
<td><p>FEC1 0048</p></td>
<td><p>:white_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>Assumed to be SH7724 before, register accesses match up with
SH7730, maybe others. See FEC1 5040 in table above, oddity.</p></td>
</tr>
<tr class="even">
<td><p><a href="{{< ref "Real-Time_Clock.md" >}}">RTC</a></p></td>
<td><p>SH7720</p></td>
<td><p>A413 FEC0</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:white_circle:</p></td>
<td><p>I've tested this, from Simon's docs</p></td>
</tr>
<tr class="odd">
<td><p><a href="RWDT">RWDT</a></p></td>
<td><p>SH7724</p></td>
<td><p>A452 0000</p></td>
<td><p>:red_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>Matches the documentation</p></td>
</tr>
<tr class="even">
<td><p><a href="CPG">CPG</a></p></td>
<td><p>SH7724</p></td>
<td><p>A415 0000</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:white_circle:</p></td>
<td><p>It is mostly understood, but there are still hardware quirks that
make using this "tricky".</p></td>
</tr>
<tr class="odd">
<td><p><a
href="{{< ref "Compare_Match_Timer.md" >}}">CMT</a></p></td>
<td><p>SH7724<br />
SH7730</p></td>
<td><p>A44A 0000</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>There are OS syscalls for interfacing with the CMT.</p></td>
</tr>
<tr class="even">
<td><p><a href="PFC">PFC</a></p></td>
<td><p>Unknown</p></td>
<td><p>A405 0100</p></td>
<td><p>:white_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>See above table for unknown accesses in the <a
href="PFC">PFC</a>.</p></td>
</tr>
<tr class="odd">
<td><p><a href="FSI">FSI</a></p></td>
<td><p>SH7724</p></td>
<td><p>FE3C 0000</p></td>
<td><p>:red_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>This is just a guess on what matched, bootloader writes to the
CLK_RST, SOFT_RST with valid values that match what it should be doing
to initialize the peripheral. Note, this is a <em>sound I/O interface, a
peripheral of the SPU2 (sound processing unit 2) audio processing
circuit</em>. There is no indication yet if the SPU2 is present. If this
is present, then there will be 2 DSPs included in the die for audio
decoding. Documentation is under an NDA, however Linux kernel code
exists to interface with this module. (Only thing I can think of that
would use this is serial transfers, but that seems wrong).</p></td>
</tr>
<tr class="even">
<td><p><a href="INTC">INTC</a></p></td>
<td><p>SH7724</p></td>
<td><p>A408 0000</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>Interrupt controller form simon's docs, USB is on INT 9.</p></td>
</tr>
<tr class="odd">
<td><p><a href="DMAC">DMAC</a></p></td>
<td><p>SH7724</p></td>
<td><p>FE00 8020</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>From simon's docs.</p></td>
</tr>
<tr class="even">
<td><p><a href="UBC">UBC</a></p></td>
<td><p>SH?????</p></td>
<td><p>FF20 0000</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>Simon has knowledge on this</p></td>
</tr>
<tr class="odd">
<td><p><a href="H-UDI">H-UDI</a></p></td>
<td><p>SH????</p></td>
<td><p>FC11 0000</p></td>
<td><p>:red_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>No pins from this have been found on the Prizm PCB yet</p></td>
</tr>
<tr class="even">
<td><p><a href="SPU2">SPU2</a></p></td>
<td><p>SH7724</p></td>
<td><p>FE2F FC00</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>Official emulator has this, FSI was detected on hardware</p></td>
</tr>
<tr class="odd">
<td><p><a href="FSI">FSI</a></p></td>
<td><p>SH7724</p></td>
<td><p>FE3C 0000</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>FSI detected</p></td>
</tr>
<tr class="even">
<td><p><a href="DSP0">DSP0</a></p></td>
<td><p>SH7724?</p></td>
<td><p>FE2F FD00</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>Official emulator has this</p></td>
</tr>
<tr class="odd">
<td><p><a href="DSP1">DSP1</a></p></td>
<td><p>SH7724?</p></td>
<td><p>FE3F FD00</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>Official emulator has this</p></td>
</tr>
<tr class="even">
<td><p>Key Interface Unit</p></td>
<td><p>SH????</p></td>
<td><p>A44B 0000</p></td>
<td><p>:large_blue_circle:</p></td>
<td><p>:red_circle:</p></td>
<td><p>Official emulator has this</p></td>
</tr>
</tbody>
</table>

## Peripherals on Cold Start {#peripherals_on_cold_start}

-   (SH7724) STBCR \<- 0 (Standby control)
-   (SH7724) MSTPCR0 \<- 0 (module stop 0)
-   (SH7724) MSTPCR0 \<- 0xFFFFFFFF (module stop 2)
-   \<snip>
-   Watchdog timer is disabled, counter set to 0, and sets the WDT clock
    to Rφ/64, or 500ms
-   Busy-loop waiting for the R64CNT register to hit != 0. (Verify it
    wants != 0)

(TODO)
