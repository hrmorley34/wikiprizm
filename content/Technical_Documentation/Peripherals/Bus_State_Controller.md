---
title: Bus State Controller
aliases:
  - /Bus_State_Controller/
---

*This page's documentation is not complete and is based off of preliminary
testing. There may be errors in the documentation or the naming conventions may
not be completely agreed upon.*

The bus state controller is an essential component of a SuperH CPU. The
BSC is tasked with mapping physical memory addresses to external memory
devices. The term *physical address* refers to a 29-bit address that the
BSC translates to physical storage. The fx-CG10/20 have an external
flash chip and DRAM chip. The BSC translates addresses into the address
space of the chips and operates the chip select pins.

## Areas

The BSC has 2 modes of operation that change how the addresses are
broken up to the various external storages. The table below lists the
areas and if the area is in use by a physical chip. The areas that are
unused seem to return repeating groups of 4 64-bit numbers with some
variation on physical hardware. The Prizm emulator has all unused areas
set to 0 for all bytes.

| Address               | Area    | Memory Device                          | Size | Used      | Notes                                                                       |
|-----------------------|---------|----------------------------------------|------|-----------|-----------------------------------------------------------------------------|
| 0000 0000 to03FF FFFF | Area 0  | Normal memory, burst rom, SRAM         | 64MB | Yes, 32MB | Due to unused address bits, the flash is repeated at 0200 0000 to 03FF FFFF |
| 0400 0000 to07FF FFFF | Area 1  | Internal IO registers                  | 64MB | No        |                                                                             |
| 0800 0000 to0BFF FFFF | Area 2  | DRAM                                   | 64MB | Yes, 2MB  | Due to unused address bits, the RAM is repeated every 2MB.                  |
| 0C00 0000 to0FFF FFFF | Area 3  | DRAM                                   | 64MB | No        |                                                                             |
| 1000 0000 to13FF FFFF | Area 4  | DRAM or normal memory, SRAM, burst ROM | 64MB | No        |                                                                             |
| 1400 0000 to13FF FFFF | Area 5A | DRAM or normal memory                  | 32MB | No        |                                                                             |
| 1600 0000 to17FF FFFF | Area 5B | DRAM or normal memory, SRAM            | 32MB | No        |                                                                             |
| 1800 0000 to19FF FFFF | Area 6A | Normal memory                          | 32MB | No        |                                                                             |
| 1A00 0000 to1BFF FFFF | Area 6B | Normal memory, SRAM                    | 32MB | No        |                                                                             |
| 1C00 0000 to1FFF FFFF | Area 7  | Reserved                               | 64MB | No        |                                                                             |
|                       |         |                                        |      |           |                                                                             |
