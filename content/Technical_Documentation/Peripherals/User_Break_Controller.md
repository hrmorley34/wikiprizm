---
title: User Break Controller
aliases:
  - /User_Break_Controller/
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

The Prizm uses the SH7724's (iirc, verify) User Break Controller. This
peripheral is used for debugging code executing on the device. A typical
usecase for this would be to set a breakpoint on the start of a specific
function and put a watchpoint for memory accesses to a buffer in RAM.
This peripheral allows for more advanced applications as well.

## Registers

## Usage

## Notes
