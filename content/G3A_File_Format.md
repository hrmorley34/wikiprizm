---
revisions:
- author: JosJuice
  comment: /\* Identification/security bytes, checksums, name \*/
  timestamp: '2015-08-06T19:02:58Z'
title: G3A_File_Format
---

.g3a files are software add-ins for the Casio Prizm. Files of this
format contain a [0x7000-byte header (28 KiB)][header-size],
and a code/data section that can be [up to 1 MB large in OS 1.01 and
1.02][maximum-size]. The maximum size of the code/data section
in newer OSes is unknown, but it seems to be larger. Checksums are used
to check whether add-ins are valid. If a part of the header isn't
documented here, it's most likely supposed to be filled with 00.

[header-size]: http://ourl.ca/8401/157437
[maximum-size]: http://ourl.ca/8207/217712

## File format {#file_format}

The overall file format is similar to those used by other Casio calculators,
such as the [g1a file used by the
fx-9860G](http://fxsdk.svn.sourceforge.net/viewvc/fxsdk/g1awrapper/trunk/g1awrapper.cpp?revision=24&view=markup).
[mkg3a]({{< ref "mkg3a.md" >}}) implements packing of data into G3A files and
can be used as an example to better understand the format.

### Identification/security bytes, checksums, name {#identificationsecurity_bytes_checksums_name}

------------------------------------------------------------------------

#### 0x0000

AA AC BD AF 90 88 9A 8D D3 FF FE FF FE FF. These bytes are found in almost every
[Prizm file](:Category:File_formats), with the exception of the ninth byte (D3),
[which is the file type byte][file-type-byte]. If the header is inverted (xor
with FF), the first eight bytes read "USBPower".

[file-type-byte]: http://ourl.ca/8207/158454

#### 0x000E

The low byte of the inverse file size (at memory location 0x0013) minus 0x41.
The actual Prizm OS doesn't care about this byte, but Manager PLUS and possibly
other programs do care, so it's important to set this byte correctly even though
the add-in can be run if it's incorrect.

#### 0x000F

This byte is always 0xFE.

#### 0x0010

**Inverse filesize** (four bytes). For example, if a file size is 00 0B 8A A8
you would xor it with FF FF FF FF, thus setting all 1's to 0's and 0's to 1's,
and get FF F4 75 57.

#### 0x0014

The low byte of the inverse file size (at memory location 0x0013) minus 0xB8.
The actual Prizm OS doesn't care about this byte, but Manager PLUS and possibly
other programs do care, so it's important to set this byte correctly even though
the add-in can be run if it's incorrect.

#### 0x0016

The word sum of 8 words (16 bytes) starting at 0x7100,
inverted.

#### 0x0020

A simple **checksum** (4 bytes long) is stored here. The checksum is calculated
based on the entire file, except for this checksum and the copy of this checksum
at the end of the file.

#### 0x0024

These two bytes are always 01 01.

#### 0x002E

**Size of executable code section** (32-bit), minus the header size.  This means
that if this is equal to 000001F8, the [copy of the 0x0020 checksum](#0x????) is
at 0x000071F8.

#### 0x0040

**Name of add-in**. This is in English, but it's not always the same as
the name at 0x006B (Picture Plot is named PictPlot at 0x0040). Probably
a shorter version of the name.

#### 0x005C

**Size of entire file** (32-bit).

#### 0x0060

**Internal name**. Only the uppercase letters A-Z and the @ character
seem to be used. @ is always written before the name. There's probably a
character limit of eight characters (including the @). Examples: \@CONV
(Conversion), \@GEOM (Geometry), \@PICTPLT (Picture Plot)

### Localized names, version information {#localized_names_version_information}

------------------------------------------------------------------------

#### 0x006B

**Name** of add-in (**English**).

#### 0x0083

**Name** of add-in (**Spanish**).

#### 0x009B

**Name** of add-in (**German**).

#### 0x00B3

**Name** of add-in (**French**).

#### 0x00CB

**Name** of add-in (**Portuguese**).

#### 0x00E3

**Name** of add-in (**Chinese**).

The font used for Chinese displays Latin letters differently than the
other languages. When making an add-in, it's a good idea to check that
the name shows up correctly when using Chinese. Long names may take up
more space than the icon does, making the names of add-ins overlap.

#### 0x00FB

**Name** of add-in, probably **reserved** for future languages. Set to
the same value as English by default.

#### 0x0113

**Name** of add-in, probably **reserved** for future languages. Set to
the same value as English by default.

#### 0x012B

Set to [0x01 for add-ins with eActivity strips][eactivity-strips], otherwise set to
0x00.

[eactivity-strips]: http://www.cemetech.net/forum/viewtopic.php?p=198680#198680

#### 0x0130

**Version number** string. Format: XX.XX.XXXX

#### 0x013C

**Date/time created** string. Format: YYYY.MMDD.HHMM

### eActivity

------------------------------------------------------------------------

This area of the add-in header describes what the add-in will look like
when viewed from an eActivity. If the add-in doesn't support eActivites,
these parts of the header should all be set to 00.

#### 0x0170

**eActivity name** of add-in (**English**).

#### 0x0194

**eActivity name** of add-in (**Spanish**).

#### 0x01B8

**eActivity name** of add-in (**German**).

#### 0x01DC

**eActivity name** of add-in (**French**).

#### 0x0200

**eActivity name** of add-in (**Portuguese**).

#### 0x0224

**eActivity name** of add-in (**Chinese**).

#### 0x0248

**eActivity name** of add-in, probably **reserved** for future
languages. Set to the same value as English by default.

#### 0x026C

**eActivity name** of add-in, probably **reserved** for future
languages. Set to the same value as English by default.

#### 0x0290

**eActivity strip icon**. The icon is 64x24 pixels large, with each
pixel represented by one nibble. This means that the icon is 0x0300
bytes long (ends at **0x058F**). Values from 0 to 7 can be used to
produce the 8 supported colors. If a value between 8 and F is used, the
pixel is displayed as black. (0 should be used if black is desired,
though.)

| Value | Color   |
|-------|---------|
| 0     | Black   |
| 1     | Blue    |
| 2     | Green   |
| 3     | Cyan    |
| 4     | Red     |
| 5     | Magenta |
| 6     | Yellow  |
| 7     | White   |

### File name, icons, executable code, checksum {#file_name_icons_executable_code_checksum}

------------------------------------------------------------------------

#### 0x0EBC

**File name** (including the .g3a extension).

#### 0x1000

**Icon** that is displayed when the add-in is **unselected**. The icon is 92x64
pixels large, and each pixel has 16 bits of color information (5 red bits, 6
green bits, and 5 blue bits). This means that the icon is 0x2E00 bytes long
(ends at **0x3DFF**).

#### 0x4000

**Icon** that is displayed when the add-in is **selected**. The icon is 92x64
pixels large, and each pixel has 16 bits of color information (5 red bits, 6
green bits, and 5 blue bits). This means that the icon is 0x2E00 bytes long
(ends at **0x6DFF**).

#### 0x7000

**Executable code section**. This section will be mapped to virtual memory at
0x00300000 and executed when the user launches the add-in.

#### 0x????

**Copy of the [0x0020](#0x0020) checksum**. These are the last four
bytes of the file. The location of these four bytes can be found by
adding 0x7000 to the value at [0x002E](#0x002E).

---

This page was copied from [the .g3a files page on
PrizmWiki](http://prizmwiki.omnimaga.org/wiki/.g3a_files) before
PrizmWiki was closed.
