---
title: {{ .Name }}
---

## Synopsis

Header
: *path to libfxcg include file that provides this function*


Syscall index
: *syscall index as appears in the libfxcg `SYSCALL` macro*

Function signature
: `void {{ .Name }}(void)`

*Add additional high-level information here*

## Parameters

param1
: unused

param2
: unused

## Returns

*Describe the value returned by this function, if needed.*

## Comments

*Add extra comments here.*

## Example

*If the syscall's use isn't obvious or is best illustrated in context, add a
code example.*

```c
int foo;
{{ .Name }}();
```